#############################################################################
##
#W  magmaut.gd  Automorphims and Isomorphisms for magmas
##  
#Y  Copyright (C)  2020                                           João Araújo
#Y                                                               Choiwah Chow
##  
##

#############################################################################
##
#O  CreamMonomorphismMagmas
##
## <#GAPDoc Label="CreamMomorphismMagmas">
##   <ManSection>
##     <Oper Name = "CreamMomorphismMagmas" Arg = "S, T"/>
##     <Returns>
##       An isomorphism between <A>S</A> and <A>T</A>, or <K>fail</K> if they are not isomorphic.
##     </Returns>
##     <Description>
##       This operation will try to find an injective homomorphism between magmas <A>S</A>
##		 and <A>T</A>. It will return the homomorphism found if there is one,
##		 and returns <K>fail</K> otherwise.
##       <P/>
##		 This operation will make use of the operation 
##		 <Ref Oper = "MAGMAS_Invariants" /> 
##		 to quickly detect those input magmas that cannot possibly be monomorphic
##		 if they have incompatible invariant vectors. It will also use the
##		 invariant vectors to limit the search space for an homomorphic mapping
##		 between the two magmas.
## 
##       <Example><![CDATA[
## gap> CreamMonomorphismMagmas(AllSmallGroups(4)[1], AllSmallGroups(4)[2]);
## fail]]></Example>
## 
##       <Example><![CDATA[
## gap> a := Group((1,2), (3,4));;
## gap> a = AllSmallGroups(4)[2];
## false
## gap> CreamMonomorphismMagmas(a, AllSmallGroups(4)[2]);
## ()]]></Example>
##	   </Description>
##   </ManSection>
## <#/GAPDoc>
DeclareOperation( "CreamMonomorphismMagmas", [ IsMagma, IsMagma ] );


#############################################################################
##
#O  CreamMonomorphismAlgebras(A, B)
##
## <#GAPDoc Label="CreamMonomorphismAlgebras">
##   <ManSection>
##     <Oper Name = "CreamMonomorphismAlgebras" Arg = "A, B"/>
##     <Returns>
##       An injective homomorphism mapping from <A>A</A> to <A>B</A>.
##     </Returns>
##     <Description>
##		 <A>A</A> and <A>B</A> are algebras of type (2, 2, 2, ..., 1, 1, 1, ..., 0, 0, ...).
##       An algebra is represented as a list of binary operations (as multiplication tables, i.e. 
##		 2-dimensional arrays), followed by unary operations, then constants (as lists):
##		 [<M>b_1</M>, <M>b_2</M>, ..., <M>u_1</M>,  <M>u_2</M>, ... <M>c_1</M>, <M>c_2</M>, ...].
##		 
##		 <P/>
##
##       <Example><![CDATA[
## gap> a := MultiplicationTable(AllSmallGroups(4)[1]);; 
## gap> b := MultiplicationTable(AllSmallGroups(4)[2]);;
## gap> A := [a, b, [1..4], [1]];;
## gap> CreamMonomorphismAlgebras(A, A);
## ()
## gap> B := [a, b, [1..4], [2]];;
## gap> CreamMonomorphismAlgebras(A, B);
## fail]]></Example>
##	   </Description>
##   </ManSection>
## <#/GAPDoc>
DeclareOperation( "CreamMonomorphismAlgebras", [ IsList, IsList ] );


#############################################################################
##
#O  CreamAllMonomorphismAlgebras(A, B)
##
## <#GAPDoc Label="CreamAllMonomorphismAlgebras">
##   <ManSection>
##     <Oper Name = "CreamMonomorphismAlgebras" Arg = "A, B"/>
##     <Returns>
##       An injective homomorphism mapping from <A>A</A> to <A>B</A>.
##     </Returns>
##     <Description>
##		 <A>A</A> and <A>B</A> are algebras of type (2, 2, 2, ..., 1, 1, 1, ..., 0, 0, ...).
##       An algebra is represented as a list of binary operations (as multiplication tables, i.e. 
##		 2-dimensional arrays), followed by unary operations, then constants (as lists):
##		 [<M>b_1</M>, <M>b_2</M>, ..., <M>u_1</M>,  <M>u_2</M>, ... <M>c_1</M>, <M>c_2</M>, ...].
##		 
##		 <P/>
##
##       <Example><![CDATA[
## gap> a := MultiplicationTable(AllSmallGroups(4)[1]);; 
## gap> b := MultiplicationTable(AllSmallGroups(4)[2]);;
## gap> A := [a, b, [1..4], [1]];;
## gap> CreamAllMonomorphismAlgebras(A, A);
## ()
## gap> B := [a, b, [1..4], [2]];;
## gap> CreamAllMonomorphismAlgebras(A, B);
## fail]]></Example>
##	   </Description>
##   </ManSection>
## <#/GAPDoc>
DeclareOperation( "CreamAllMonomorphismAlgebras", [ IsList, IsList ] );


#############################################################################
##  Undocumented auxiliary functions/operations - for internal use only
##


DeclareGlobalFunction( "CreamAllMonomorphismMagmasNC" );
DeclareGlobalFunction( "CREAM_ExtendAllMonomorphisms" );
DeclareGlobalFunction( "CREAM_CompatibleInvariants" );

