#############################################################################
##
#W  Cream.gi  The CREAM Package   Rui Barradas Pereira
##
##  Installation file for functions of the CREAM package.
##
##

CreamMace4Filename := Filename( DirectoriesPackageLibrary("Cream", "src/prover9/build"), "mace4" );
if CreamMace4Filename = fail then
	CreamMace4Filename := Filename( DirectoriesSystemPrograms(), "mace4" );
fi;

#############################################################################
##
##  partition -  is a grouping of the set's elements into non-empty subsets, 
##               in such a way that every element is included in exactly one 
##               subset. (Wikipedia "Partition of a set")
##
##
##  In this package a partition is represented as a forest of trees using an  
##  underlying array of size n (the size of the set/partition). Each position 
##  of array the represents an element of the set (node in the tree). if the 
##  value of the position is positive then its value point to the parent node.
##  If the value is negative then the node is the root of a tree and the 
##  its absolute value is the number of children under that root.
##
##  Under this representation the trees standing for a block in a partition 
##  can have more than one level and any of the elements of the block can
##  be the root of the block.
##
##  Under this representation a normalized partition is one such that the 
##  trees are shallow (each node is either the root or points to the root)
##  and the root is always the smaller order element. This allows for a 
##  bijective mapping between partitions and its representation and the 
##  use of array comparison functions to compare partitions. 


#############################################################################
##
#F  isPartition([<partition>]) 
##
##	Determines whether partition is valid partition. For a partition to be 
##  valid no partition element can "belong" to a partition whose root is 
##  bigger than the dimension (partition[i] > dimension) or is zero 
##  (partition[i] = 0) 
##
isPartition := function( partition )

	local dimension,i,r;
	
	dimension := Length (partition);
	for i in [1 .. dimension] do
		if partition[i] > dimension or partition[i] = 0 then 
			return false;
		fi;
	od;
	
	return true;

end;


#############################################################################
##
#F  CreamSizeAlgebra ([<algebra>])   
##
##  algebraSize returns the algebra order. For determining the order, it also
##  determines whether the algebra is valid. An algebra will be valid if all
##  the functions of the algebra have the same order. algebraSize will 
##  return fail if the algebra is not valid.
##
InstallGlobalFunction( CreamSizeAlgebra, function( algebra )

	local size, cSize, func, elm, lst;
	
	size := -1;
	
	if not IsList (algebra) then
		return fail;
	fi;
	
	for func in algebra do
		if IsList (func) then
		
			if IsRectangularTable (func) then
		
				cSize := DimensionsMat(func);
				if not cSize[1] = cSize[2] then
					return fail;
				fi;
			
				if (size = -1) then
					size := cSize[1];
				elif not size = cSize[1] then
					return fail;
				fi;

				for lst in func do
					for elm in lst do
						if not (IsPosInt(elm) and (elm <= size)) then
							return fail;
						fi;
					od;
				od;

			else
				
				cSize := Length (func);
				if (size = -1) then
					size := cSize;
				elif not size = cSize then
					return fail;
				fi;
			
				for elm in func do
					if not (IsPosInt(elm) and (elm <= size)) then
						return fail;
					fi;
				od;
			
			fi;		
		
		else
		
			return fail;		
			
		fi;
	od;
	
	return size;

end );


#############################################################################
##
#F  internalSizeAlgebra ([<algebra>])  . . . . . . . . . .return algebra size   
##
##  internalSizeAlgebra returns the algebra order assuming that the algebrs
##  is algebra. Under thia assumption this is simply the lenght of vector
##  that encodes one of the algebra's function. For simplicity the length of
##  the first is used. This function should only be used after verifying that 
##  an algebra is valid using sizeAlgebra
##
internalSizeAlgebra := function( algebra )
	
	return Length (algebra[1]);

end;


#############################################################################
##
#F  CreamIsAlgebra ([<algebra>])
##
##  CreamIsAlgebra tests if an algebra is valid using intenally sizeAlgebra. If 
##  sizeAlgebra fails then isAlgebra returns false otherwise returns true. 
##
InstallGlobalFunction( CreamIsAlgebra, function( algebra )

	local size;
	
	size := CreamSizeAlgebra (algebra);
	
	if size = fail then
		return false;
	else 
		return true;
	fi;

end );

#############################################################################
##
#F  internalConvertPartition ([partition])   
##
##  internalConvertPartition converts the partition from the normalized tree 
##  representation to a more human-readable representation list of classes 
##  in which each class is represented as the list of elements of the class
##
internalConvertPartition := function( partition )

	local hrpartition,spart,idx,block;

	spart := Size (partition);
	hrpartition := [];

	for idx in [1 .. spart] do
		if partition[idx] < 0 then
			Add (hrpartition, [idx]);
		else
			for block in [1 .. Size (hrpartition)] do
				if hrpartition[block][1] = partition[idx] then
					Add (hrpartition[block],idx);
					break;
				fi;
			od;
		fi; 
	od;	
	
	return hrpartition;

end;

#############################################################################
##
#F  CreamConvertPartition ([<partition>])
##
##  CreamConvertPartition converts the partition from the normalized tree 
##  representation to a more human-readable representation list of classes 
##  in which each class is represented as the list of elements of the class
##
InstallGlobalFunction( CreamConvertPartition, function( partition )

	if isPartition (partition) then
		return internalConvertPartition (partition);
	else 
		return fail;
	fi;

end );

#############################################################################
##
#F  internalUnaryFunctionsAlgebra ([<algebra>])
##
##  Converts binary functions in algebras to unary functions. The matrix in
##  binary functions is decoupled into its lines and columns.
##
internalUnaryFunctionsAlgebra := function( algebra )

	local size, ufunc, func, ualgebra;
	
	ualgebra := [];
	size := internalSizeAlgebra(algebra);
	
	if size = fail then
		return fail;
	else
		for func in algebra do
			if IsRectangularTable (func) then
				for ufunc in func do
					AddSet(ualgebra,ufunc);
				od;
				for ufunc in TransposedMat( func ) do
					AddSet(ualgebra,ufunc);
				od;
			else
				AddSet(ualgebra,func);
			fi;
		od;
	fi;
	
	return ualgebra;

end;

#############################################################################
##
#F  unaryFunctionsAlgebra ([<algebra>]) 
##
##  Converts binary functions in algebras to unary functions.
##
unaryFunctionsAlgebra := function( algebra )

	local size;
	
	size := CreamSizeAlgebra(algebra);
	
	if size = fail then
		return fail;
	else
		return internalUnaryFunctionsAlgebra (algebra);
	fi;

end;

#############################################################################
##
#F  internalPrincipalCongruence( <algebra>, <pair> ) . find congruences in algebra
##
##  Returns the principal congruence generated by a pair of elements based on 
##  the Ralph Freese algorithm described in "Computing congruences efficiently".
##  The congruence is returned in the partition format described above.
##
internalPrincipalCongruence := function (ualgebra, pair)

	local dimension, internal_partition;
	
	dimension := internalSizeAlgebra(ualgebra);
		
	internal_partition := cPrincipalCongruence(ualgebra, pair);
	
	return internal_partition;
	
end;

#############################################################################
##
#F  CreamPrincipalCongruence( <alg>, <cong> )
##
##  Returns the principal congruence generated by a pair of elements
##
InstallGlobalFunction( CreamPrincipalCongruence, function (algebra, pair)

	local size, initial_pair, ualgebra, congruences;
	
	size := CreamSizeAlgebra(algebra);
	
	if size = fail then
		return fail;
	fi;	

	if not IsList(pair) then
		return fail;
	fi;

	if Length(pair) <> 2 then
		return fail;
	fi;	
	
	initial_pair := Set (pair);

	# Verifies pair
		
	if Length(initial_pair) <> 2 then
		return fail;
	fi;	
	
	# The algoritm uses only unary functions. Binary functions are converted into
	# unary functions 

	ualgebra := internalUnaryFunctionsAlgebra(algebra);

	congruences := internalPrincipalCongruence(ualgebra, initial_pair);
	
	return congruences;
	
end );

#############################################################################
##
#F  addCongruence2List( <cong>, <list>, <start> )
##
##  Adds a congruence encoded as a partition to a ordered list. The 
##  previously defined comparePartitions function is used to keep the list
##  ordered. Uses binary search to determine new element position in the 
##  list. The optional start parameter limits the search to on or after the
##  start position.
##
addCongruence2List := function (congruence, listCongruences, start...)

	local l,i,r,st,comp;
	
	# Set start depending on optional paramenter
	
	if Length(start) = 0 then
		st := 1;
	else 
		st := start[1];
	fi;
	
	#l := st;
	i := st;
	r := Length (listCongruences);
	
	# Test start position
	
	if r < st then
		comp := 1;
		i := r;
		l := st;
	else
		comp := cComparePartitions (listCongruences[i]);
		if comp < 0 then
			r := i - 1;
			l := i;
		elif comp > 0 then
			l := i + 1;
		fi;
	fi;
	
	# Do a binary search for the position
	
	while comp <> 0 and l <= r do
		#i := QuoInt (l + r, 2);
		i := cAverage (l, r);
		comp := cComparePartitions (listCongruences[i]);
		if comp < 0 then
			r := i - 1;
		elif comp > 0 then
			l := i + 1;
		fi;
	od;

	# Add congruence to the found position
	
	if comp = -1 then
		Add (listCongruences,congruence,i);
	elif comp = 1 then
		Add (listCongruences,congruence, i + 1);
	fi;

end;

#############################################################################
##
#F  CreamNumberOfBlocks( <part> )
##
##  Returns the number of blocks in a partition
##
InstallGlobalFunction( CreamNumberOfBlocks, function (partition)

	local n,p;
	
	if isPartition (partition) then

		n := 0;
		for p in partition do
			if p < 0 then
				n := n + 1;
			fi;
		od;
	
		return n;
	else 
		return fail;
	fi;

end );

#############################################################################
##
#F  internalAllPrincipalCongruences( <algebra> )
##
##  Returns all principal congruences of an algebra. This is done by going 
##  through all possible pairs in a algebra and calculating the principal 
##  congruence for each one of these pairs. 
##
internalAllPrincipalCongruences := function (algebra)

	local dimension, allPriCongruencesList, ualgebra ;

	dimension := internalSizeAlgebra(algebra); 
	
	ualgebra := internalUnaryFunctionsAlgebra(algebra);

	allPriCongruencesList := cAllPrincipalCongruences(ualgebra);
		
	return allPriCongruencesList;

end;

#############################################################################
##
#F  CreamAllPrincipalCongruences( <algebra> )
##
##  Returns all principal congruences of an algebra.
##
InstallGlobalFunction( CreamAllPrincipalCongruences, function (algebra)

	local size, ualgebra;
	
	size := CreamSizeAlgebra(algebra);
	
	if size = fail then
		return fail;
	else
		return internalAllPrincipalCongruences(algebra);
	fi;

end );

#############################################################################
##
#F  internalContainedPartition( <cong1> , <cong2> )
##
##  Returns whether partition cong1 is contained in cong2. This means that 
##  every element in same block in cong1 will also be in the same block in 
##  cong2 
##
internalContainedPartition := function (cong1,cong2)

	local dimension, i, j, part2;
	
	dimension := Size(cong1); 

	for i in [1 .. dimension] do

		# For each block...
	
		if cong1[i] < 0 then
	
			if cong2[i] < 0 then
				part2 := i;
			else 
				part2 := cong2[i];
			fi;
			
			# ... check whether each block element in in the same block in cong2
			
			for j in [i+1 .. dimension] do
				if cong1[j] = i and not part2 = cong2[j] then
				
					# return false if find cong1 elements in different cong2 blocks
					
					return false;
				fi;
			od;
		fi;
	od;
	
	return true;

end;

#############################################################################
##
#F  CreamContainedPartition( <cong1> , <cong2> )
##
##  Returns whether partition cong1 is contained in cong2.
##
InstallGlobalFunction( CreamContainedPartition, function (cong1,cong2)

	local size;
	
	if isPartition(cong1) = false or isPartition(cong1) = false then
		return fail;
	else
		return internalContainedPartition(cong1,cong2);
	fi;

end );

#############################################################################
##
#F  internalContainedPartitions( <algebra> )
##
##  Returns the list of all congruence that don't contain any other congruence.
##  If a single congruence is returned then this congruence is contained
##  in every other conguence (and the algebra is monolithic)
##
internalContainedPartitions := function (algebra)

	local congs, ncongs, i;

	# Calculates all principal conguences
	
	congs := internalAllPrincipalCongruences (algebra);
	
	ncongs := Size(congs); 
	
	# Congruences are tested and if a congruence contains another is removed 
	# from the list
	
	if ncongs > 1 then
		i := 2;
		repeat
			if internalContainedPartition (congs[1],congs[i]) then
				Remove (congs,i);
			elif internalContainedPartition (congs[i],congs[1]) then
				congs[1] := congs[i];
				Remove (congs,i);
				i := 2;
			else
				i := i + 1;
			fi;
		until Size (congs) < i;  
		
	fi;
	
	return congs;
	
end;

#############################################################################
##
#F  internalIsAlgebraMonolithic( <algebra> )
##
##  Determines whether an algebra is monolithic. This is done by calling the
##  function internalContainedPartitions that returns all congruences that 
##  don't contain any other congruence. If a single congruence is returned 
##  then this congruence is contained in every other conguence (and the 
##  algebra is monolithic)
##
internalIsAlgebraMonolithic := function (algebra)

	local congs, ncongs, i;

	congs := internalContainedPartitions (algebra);
	
	ncongs := Size(congs); 
	
	if ncongs = 0 then
		return false;
	elif ncongs = 1 then
		return true;
	else
		return false;
	fi;
	
end;

#############################################################################
##
#F  CreamIsAlgebraMonolithic( <algebra> )
##
##  Determines whether an algebra is monolithic.
##
InstallGlobalFunction( CreamIsAlgebraMonolithic, function (algebra)
	
	local size;
	
	size := CreamSizeAlgebra(algebra);
	
	if size = fail then
		return fail;
	else
		return internalIsAlgebraMonolithic(algebra);
	fi;

end );


#############################################################################
##
#F  internalAllCongruences( <alg> )
##
##  Returns all congruences of an algebra. Initially all principal congruences
##  all calculated. The trivial congruence is added to this list. The principal
##  congruences are combined to generate all possible congruences.
##
internalAllCongruences := function (algebra)

	local i, allCongruencesList, ualgebra, dimension, singletonCongruence;
	
	# All the congruences are generated
	
	dimension := internalSizeAlgebra(algebra);

	ualgebra := internalUnaryFunctionsAlgebra(algebra);
	allCongruencesList := cAllCongruences(ualgebra);

	# The singleton congruence is created and added to the congruences list	
	
	singletonCongruence:=[1 .. dimension];
	for i in [1 .. dimension] do
		singletonCongruence[i]:=-1;
	od;

	Add (allCongruencesList, singletonCongruence, 1);
	
	return allCongruencesList;

end;

#############################################################################
##
#F  CreamAllCongruences( <algebra> )
##
##  Returns all congruences of an algebra.
##
InstallGlobalFunction( CreamAllCongruences, function (algebra)

	local size, congruences;
	
	size := CreamSizeAlgebra(algebra);
	
	if size = fail then
		return fail;
	else
		return internalAllCongruences(algebra);
	fi;

end );

#############################################################################
##
#F  mace4Header( <input>, <dimension>, <max_model> )
##
##  Return mace4 header. Takes the size of the algebra for domain size and
##  the maximum number of models to be returned by mace4. With -1 it will
##  return all models.
##
mace4Header := function (input, dimension, max_model)
	
	Append(input, Concatenation("assign(domain_size,",String(dimension),").\n"));
	Append(input, Concatenation("assign(start_size,",String(dimension),").\n"));
	Append(input, Concatenation("assign(end_size,",String(dimension),").\n"));
	Append(input, Concatenation("assign(max_models,",String(max_model),").\n"));
	Append(input, Concatenation("assign(max_megs,",String(500),").\n"));
	Append(input, "formulas(assumptions).\n");
	
end;

#############################################################################
##
#F  mace4AlgebraNamedOp( <input>, <algebra> )
##
##  Returns the algebra representation in mace4
## 
mace4AlgebraNamedOp := function (input, algebra, op)

	local nops, dimension, i, j, k;
	
	nops := Length (algebra);
	dimension := Length (algebra[1]);
	
	for i in [1 .. nops] do
		if IsRectangularTable (algebra[i]) then
			
			# Representation of a binary function in mace4
			
			for j in [1 .. dimension] do
				for k in [1 .. dimension] do
					Append (input, Concatenation ("fa", op, String(i),"(",String(j-1),",",String(k-1),")=",String(algebra[i][j][k]-1),".\n"));
				od;
			od;
		else
		
			# Representation of an unary function in mace4
		
			for j in [1 .. dimension] do
				Append(input, Concatenation ("fa", op, String(i),"(",String(j-1),")=",String(algebra[i][j]-1),".\n"));
			od;
		fi;
	od;
	
end;

#############################################################################
##
#F  mace4QuotientAlgebraAModel( <input>, <algebra> , <cong> )
##
##  Returns the quotient algebra representation in mace4. In the quotient algebra
##  elemtent are substituted by its root 
##
mace4QuotientAlgebraAModel := function (input, algebra, cong)

	local nops, dimension, i, j, k;
	
	nops := Length (algebra);
	dimension := Length (algebra[1]);
	
	for i in [1 .. nops] do
		if IsRectangularTable (algebra[i]) then
		
			# Representation of a binary function in mace4		
		
			for j in [1 .. dimension] do
				for k in [1 .. dimension] do
					if cong[j] < 0 and cong[k] < 0 then
						if j < k then
							Append(input, Concatenation("a",String(j-1),"!=a",String(k-1),".\n"));
						fi;
						Append(input, Concatenation("fa", String(i),"(a",String(j-1),",a",String(k-1),")=a",String(cRootBlock(algebra[i][j][k],cong)-1),".\n"));
					fi;
				od;
			od;		
		else
		
			# Representation of an unary function in mace4
		
			for j in [1 .. dimension] do
				if cong[j] < 0 then
					Append(input, Concatenation ("fa", String(i),"(a",String(j-1),")=a",String(cRootBlock(algebra[i][j],cong)-1),".\n"));			
				fi;
			od;
		fi;
	od;	
	
end;


#############################################################################
##
#F  mace4QuotientAlgebraOp( <input>, <algebra> , <cong> )
##
##  Returns the quotient algebra representation in mace4. In the quotient algebra
##  elemtent are substituted by its root 
##
mace4QuotientAlgebraOp := function (input, algebra, cong)

	local nops, dimension, i, j, k;
	
	nops := Length (algebra);
	dimension := Length (algebra[1]);
	
	for i in [1 .. nops] do
		if IsRectangularTable (algebra[i]) then
		
			# Representation of a binary function in mace4		
		
			for j in [1 .. dimension] do
				for k in [1 .. dimension] do
					if cong[j] < 0 and cong[k] < 0 then
						Append(input, Concatenation("fq", String(i),"(",String(j-1),",",String(k-1),")=",String(cRootBlock(algebra[i][j][k],cong)-1),".\n"));
					fi;
				od;
			od;		
		else
		
			# Representation of an unary function in mace4
		
			for j in [1 .. dimension] do
				if cong[j] < 0 then
					Append(input, Concatenation ("fq", String(i),"(",String(j-1),")=",String(cRootBlock(algebra[i][j],cong)-1),".\n"));			
				fi;
			od;
		fi;
	od;	
	
end;


#############################################################################
##
#F  mace4SubAlgebraNamedOp( <input>, <algebra> , <sub> , op, size)
##
##  Returns the subalgebra representation in mace4. argument op is a string 
##  identifying the subalgebras functions. size is normally the size of the
##  of the subalgebra but can be bigger and the function is zeroed above the
##  dimension of the subalgebra.
##
mace4SubAlgebraNamedOp := function (input, algebra, sub, op, size)

	local nops, dimension, subDimension, i, j, k;	
	
	nops := Length (algebra);
	dimension := Length (algebra[1]);
	subDimension := Length (sub);
	
	for i in [1 .. nops] do
		if IsRectangularTable (algebra[i]) then
		
			# Representation of a binary function in mace4
			
			for j in [1 .. size] do
				for k in [1 .. size] do			
					if j <= subDimension and k <= subDimension then
						Append (input, Concatenation("f",op, String(i),"(",String(j-1),",",String(k-1),")=",String(Position (sub,algebra[i][sub[j]][sub[k]])-1),".\n"));
					else
						Append(input, Concatenation("f",op, String(i),"(",String(j-1),",",String(k-1),")=",String(0),".\n"));					
					fi;				
				od;
			od;		
		else
		
			# Representation of an unary function in mace4
		
			for j in [1 .. size] do
				if j <= subDimension then
					Append(input, Concatenation("f",op, String(i),"(",String(j-1),")=",String(Position (sub,algebra[i][sub[j]])-1),".\n"));
				else
					Append(input, Concatenation("f",op, String(i),"(",String(j-1),")=",String(0),".\n"));					
				fi;				
			od;
		fi;
	od;	
	
end;

#############################################################################
##
#F  mace4SubAlgebraOp( <input>, <algebra> , <sub>, <size> )
##
##  Returns the subalgebra representation in mace4.
##
mace4SubAlgebraOp := function (input, algebra, sub, size)

	mace4SubAlgebraNamedOp (input, algebra, sub, "s", size);
	
end;

#############################################################################
##
#F  mace4MapQuotient2Algebra( <input>, <algebra> )
##
##  Returns the mapping from quotient function to algebra
##
mace4MapQuotient2Algebra := function (input, algebra)

	local nops, i;
	
	nops := Length (algebra);	
	
	Append(input, "f(x) = f(y) -> x = y.\n");
	for i in [1 .. nops] do
		if IsRectangularTable (algebra[i]) then
		
			# Mapping for a binary function
		
			Append(input, Concatenation("f(fq", String(i), "(x,y)) = fa", String(i), "(f(x),f(y)).\n"));
		else
		
			# Mapping for an unary function
		
			Append(input, Concatenation("f(fq", String(i), "(x)) = fa", String(i), "(f(x)).\n"));
		fi;
	od;
	
end;

#############################################################################
##
#F  mace4Injection( <input>, <nops>, <g>, <h> )
##
##  Returns an injective mapping between g and h. algebra is one of the 
##  algebras representing the structure of algebras being mapped and g 
##  and h are strings representing the undelying algebras
##
mace4Injection := function (input,algebra,g,h)

	local i, op, nops;
	
	nops := Size (algebra);

	for i in [1 .. nops] do
		if IsRectangularTable (algebra[i]) then
			Append(input, Concatenation("f(f", String(g), String(i), "(x,y)) = f", String(h), String(i), "(f(x),f(y)).\n"));
		else
			Append(input, Concatenation("f(f", String(g), String(i), "(x)) = f", String(h), String(i), "(f(x)).\n"));
		fi;
	od;
	Append(input, "f(x) = f(y) -> x = y.\n");
	
end;

#############################################################################
##
#F  mace4MapEndomorphism( <input>, <algebra>, <sub>, <cong> )
##
##  Returns the representation of an endomorphism from algebra to subalgebra
##  considering an underliying congruence.
##
mace4MapEndomorphism := function (input, algebra, sub, cong)

	local nops, dimension, subDimension, i, j;

	nops := Length (algebra);
	dimension := Length (cong);
	subDimension := Length (sub);
	
	# Constrains the endomorphisms to the congruence
	
	for i in [1 .. dimension - 1] do
		for j in [i+1 .. dimension] do
			if (cong[i] < 0 and i = cong[j]) or (cong[i] > 0 and cong[i] = cong[j]) then 
				Append(input, Concatenation("f(",String(i-1),")=f(",String(j-1),").\n"));
			else 
				Append(input, Concatenation("f(",String(i-1),")!=f(",String(j-1),").\n"));
			fi;
		od;
	od;	

	# Map from algebra to subalgebra
	
	for i in [1 .. dimension] do
		for j in [1 .. subDimension] do
			if cong[i] < 0 then
				if not j=1 then
					Append(input, "|");
				fi;
				Append(input, Concatenation("f(",String(i-1),")=",String(j-1)));
				if j=subDimension then
					Append(input, ".\n");
				fi;
			fi;
		od;
		# Can put dot here instead of above
		# input := Concatenation(input, ".\n");
	od;	

	# Constrain the endomorphisms considering the algebra operations and respective subalgebra operations

	for i in [1 .. nops] do
		if IsRectangularTable (algebra[i]) then
			Append(input, Concatenation("f(fa",String(i),"(x,y))=fs",String(i),"(f(x),f(y)).\n"));
		else
			Append(input, Concatenation("f(fa",String(i),"(x))=fs",String(i),"(f(x)).\n"));
		fi;
	od;
	
end;

#############################################################################
##
#F  mace4Endomorphisms( <input>, <partition> )
##
##  Get all algebra endomorphisms  
##
mace4Endomorphisms := function (input, algebra)

	local nops, i;

	nops := Length (algebra);

	for i in [1 .. nops] do
		if IsRectangularTable (algebra[i]) then
			Append(input, Concatenation("f(fa",String(i),"(x,y))=fa",String(i),"(f(x),f(y)).\n"));
		else
			Append(input, Concatenation("f(fa",String(i),"(x))=fa",String(i),"(f(x)).\n"));
		fi;
	od;
	
end;

#############################################################################
##
#F  mace4PartitionEndomorphisms( <input>, <partition> )
##
##  Limits possible endomorphisms according to a partition. Each block  
##  maps to the same element
##
mace4PartitionEndomorphisms := function (input, algebra, partition)

	local dimension, nops, i, j;

	dimension := Length (partition);
	nops := Length (algebra);

	for i in [1 .. dimension - 1] do
		for j in [i+1 .. dimension] do
			if cRootBlock(i,partition) = cRootBlock(j,partition) then 
				Append(input, Concatenation("f(",String(i-1),")=f(",String(j-1),").\n"));
			else 
				Append(input, Concatenation("f(",String(i-1),")!=f(",String(j-1),").\n"));
			fi;
		od;
	od;

	for i in [1 .. nops] do
		if IsRectangularTable (algebra[i]) then
			Append(input, Concatenation("f(fa",String(i),"(x,y))=fa",String(i),"(f(x),f(y)).\n"));
		else
			Append(input, Concatenation("f(fa",String(i),"(x)=fa",String(i),"(f(x)).\n"));
		fi;
	od;
	
end;

#############################################################################
##
#F  mace4PartitionLimitAlgebraAutomorphism( <input>, <partition> )
##
##  Limits possible automorphisms according to a partition. Automorphism 
##  mappings are possible inside blocks
##
mace4PartitionLimitAlgebraAutomorphism := function (input, partition)

	local dimension, i, j, firstPositive;

	dimension := Length (partition);
	
	for i in [1 .. dimension] do
		firstPositive := true;
		for j in [1 .. dimension] do
			if cRootBlock(i,partition) = cRootBlock(j,partition) then
			
				# Mappings between elements of the same block are possible 
			
				if firstPositive then
					firstPositive := false;
				else
					Append(input, "|");
				fi;
				Append(input, Concatenation("f(",String(i-1),")=",String(j-1)));
			fi;
		od;
		Append(input, ".\n");
	od;
	
end;

#############################################################################
##
#F  mace4IsAlgebraAutomorphism( <input> , <algebra> , <auto> )
##
##  Test whether a mapping auto is an automorphism 
##
mace4IsAlgebraAutomorphism := function (input, algebra, auto)

	local nops, dimension, i, j;

	dimension := internalSizeAlgebra (algebra);
	
	for i in [1 .. dimension] do
		if not i=1 then
			Append(input, "&");
		fi;
		
		# Only the mapping auto is possible
		
		Append(input, Concatenation("f(",String(i-1),")=",String(auto[i]-1)));
		if i=dimension then
			Append(input, ".\n");
		fi;
	od;
	
end;

#############################################################################
##
#F  mace4MapAutomorphisms( <input> , <algebra> )
##
##  Generic automorphism mace4 clauses
##
mace4MapAutomorphisms := function (input, algebra)

	local nops, dimension, i, j;

	nops := Length (algebra);
	dimension := internalSizeAlgebra (algebra);
	
	# Mapping is bijective 
	
	for i in [1 .. dimension - 1] do
		for j in [i+1 .. dimension] do
			Append(input, Concatenation("f(",String(i-1),")!=f(",String(j-1),").\n"));
		od;
	od;			

	# Mapping respects properties of every operation

	for i in [1 .. nops] do
		if IsRectangularTable (algebra[i]) then
			Append(input, Concatenation("f(fa",String(i),"(x,y))=fa",String(i),"(f(x),f(y)).\n"));
		else
			Append(input, Concatenation("f(fa",String(i),"(x))=fa",String(i),"(f(x)).\n"));
		fi;
	od;
	
end;

#############################################################################
##
#F  mace4ClassAxioms( <input> , <class> )
##
##  Generic class axioms mace4 clauses
##
mace4ClassAxioms := function (input, class)

	Append(input, class);
	
end;

#############################################################################
##
#F  mace4Footer( <input> )
##
##  Returns mace4 Footer
##
mace4Footer := function (input)
	
	Append(input, "end_of_list.\n");
	Append(input, "formulas(goals).\n");
	Append(input, "end_of_list.\n");
	
end;

#############################################################################
##
#F  mace4IsFModel( <output> )
##
##  Return true if there is a fmodel in mace4 output
##
mace4IsFModel := function (output)

	local pos;
	
	pos := PositionSublist (output,"function(f(_), ",1);
	if pos = fail then
		return false;
	else
		return true;
	fi;
	
end;

#############################################################################
##
#F  mace4GetFModel( <output>, <pos> )
##
##  Returns a model for f() and its postition from mace4 output. The function
##  starts searching in position pos and returns the first f() model and the
##  position after the returned model allowing for iteractivelly search for
##  all f() models.
##
mace4GetFModel := function (output, pos)

	local posS, fmodel;
	
	posS := PositionSublist (output,"function(f(_), ",pos);
	if posS = fail then
		fmodel := fail;
	else
		pos := PositionSublist (output,"]",posS);
		fmodel := EvalString(output{[posS+15 .. pos]});
		fmodel := List (fmodel, x->x+1);
	fi;
	
	return [fmodel, pos];
	
end;

#############################################################################
##
#F  mace4GetAModel( <output>, <pos> )
##
##  Returns a model for ax and its postition from mace4 output. The function
##  starts searching in position pos and returns the first ax model and the
##  position after the returned model allowing for iteractivelly search for
##  all ax models.
##
mace4GetAModel := function (output, pos, dimension)

	local posS, posE, i, amodel;
	
	if pos = fail then
		posS := fail;
	else 
		posS := PositionSublist (output,"function(a",pos);
	fi;
	if posS = fail then
		amodel := fail;
		posE := fail;
	else
		amodel := [];
		for i in [1 .. dimension] do
			amodel[i] := 0;
		od;
		posE := PositionSublist (output,"interpretation",posS);
		repeat 
			pos := PositionSublist (output,",",posS);
			i := EvalString(output{[posS+10 .. pos-1]}) + 1;
			posS := PositionSublist (output,"[",pos);
			pos := PositionSublist (output,"]",posS);
			amodel[i] := EvalString(output{[posS+1 .. pos-1]}) + 1;
			posS := PositionSublist (output,"function(a",pos);
		until posS = fail or posS > posE;
	fi;
	
	return [amodel, posE];
	
end;

#############################################################################
##
#F  mace4GetAlgebraModel( <output>, <pos> )
##
##  Returns a model for algebra and its postition from mace4 output. The function
##  starts searching in position pos and returns the first algebra model and the
##  position after the returned model allowing for iteractivelly search for
##  all algebra models.
##
mace4GetAlgebraModel := function (output, pos, dimension)

	local posS, posE, malgebra, algebra, fn, f, i, j;
	
	malgebra := [];
	if pos = fail then
		posS := fail;
	else 
		posS := PositionSublist (output,"function(",pos);
	fi;
	if posS = fail then
		algebra := fail;
		posE := fail;
	else
		posE := PositionSublist (output,"interpretation",posS);
		repeat 
			posS := PositionSublist (output,"[",posS);
			pos := PositionSublist (output,"]",posS);
			Add (malgebra, EvalString(output{[posS .. pos]}));
			posS := PositionSublist (output,"function(",pos);
		until posS = fail or posS > posE;
	fi;

	malgebra := Filtered (malgebra, x -> not (Size (x) = 1));

	algebra := [];
	fn := Size (malgebra);
	for f in [1 .. fn] do
		if Size (malgebra[f]) = dimension then
			algebra[f] := List (malgebra[f], x->x+1);
		else
			algebra[f] := [];
			for i in [1 .. dimension] do
				algebra[f][i] := [];
				for j in [1 .. dimension] do
					algebra[f][i][j] := malgebra[f][(i-1)*dimension+j]+1;
				od;
			od;
		fi;	
	od;

	if fn = 0 then
		algebra := fail;
	fi;
	
	return [algebra, posE];
	
end;

#############################################################################
##
#F  subFromFModel( <fmodel>, <cong> )  
##
##  Returns a subalgebra from a f() model and congruence
##
subFromFModel := function (fmodel, cong)

	local dimension, i, sub;
	
	dimension := Length (fmodel);
	
	sub := [];
	for i in [1..dimension] do
		if cong[i]<0 then
			AddSet (sub,fmodel[i]);
		fi;
	od;
	
	return sub;
	
end;

#############################################################################
##
#F  endoFromFModel( <fmodel>, <sub> )
##
##  Returns endomorphism from fmodel and subalgebra  
##
endoFromFModel := function (fmodel, sub)

	fmodel := List (fmodel, x->sub[x]);
	
	return fmodel;
	
end;

#############################################################################
##
#F  internalAreAlgebrasIsomorphicMace4( <algebra1>, <algebra2> )
##
##  Returns whether 2 subalgebras are isomorphic
##
internalAreAlgebrasIsomorphicMace4 := function (algebra1, algebra2)

	local nops, dimension, dir, input, output, istream, ostream;
	
	nops := Length(algebra1);
	dimension := internalSizeAlgebra(algebra1);
	
	dir := DirectoryCurrent();
			
	# Build mace4 input

	input := [];
	mace4Header (input, dimension,1);
	mace4AlgebraNamedOp(input, algebra1, "s");
	mace4AlgebraNamedOp(input, algebra2, "t");
	mace4Injection(input, algebra1,"as","at");
	mace4Footer(input);
			
	istream := InputTextString( input );
	
	output := ""; 
	ostream := OutputTextString(output,true);
			
	# Run mace4 with input
	
	Process( dir, CreamMace4Filename, istream, ostream, [] );

	# If a model is found return true
		
	return mace4IsFModel (output);

end;

#############################################################################
##
#F  CreamAreIsomorphicAlgebrasMace4( <algebra1>, <algebra2> )
##
##  This function returns true if the algebras A and B are isomorphic
##
InstallGlobalFunction( CreamAreIsomorphicAlgebrasMace4, function (algebra1, algebra2)

	local dimension1, dimension2;
	
	dimension1 := CreamSizeAlgebra(algebra1);
	dimension2 := CreamSizeAlgebra(algebra2);
	
	if (dimension1 = fail) or (dimension2 = fail) then
		return fail;
	elif not dimension1 = dimension2 then
		return false;
	elif not Length (algebra1) = Length (algebra2) then
		return false;
	else 
		return internalAreAlgebrasIsomorphicMace4 (algebra1, algebra2);
	fi;

end );

#############################################################################
##
#F  internalAreAlgebrasIsomorphic( <algebra1>, <algebra2> )
##
##  Returns whether 2 subalgebras are isomorphic
##
internalAreAlgebrasIsomorphic := function (algebra1, algebra2)

	return not IsomorphismAlgebras (algebra1,algebra2) = fail;

end;

#############################################################################
##
#F  CreamAreIsomorphicAlgebras( <algebra1>, <algebra2> )
##
##  This function returns true if the algebras A and B are isomorphic
##
InstallGlobalFunction( CreamAreIsomorphicAlgebras, function (algebra1, algebra2)

	local dimension1, dimension2;
	
	dimension1 := CreamSizeAlgebra(algebra1);
	dimension2 := CreamSizeAlgebra(algebra2);
	
	if (dimension1 = fail) or (dimension2 = fail) then
		return fail;
	elif not dimension1 = dimension2 then
		return false;
	elif not Length (algebra1) = Length (algebra2) then
		return false;
	else 
		return internalAreAlgebrasIsomorphic (algebra1, algebra2);
	fi;

end );

#############################################################################
##
#F  internalNonIsomorphicAlgebras( <algebra_list> )
##
##  Returns the list of isomorphic algebras
##
internalNonIsomorphicAlgebras := function (algebra_list)

	local iso_algebra, iso, i, j;

	iso_algebra := [algebra_list[1]];

	for i in [2..Size(algebra_list)] do
		iso := false;
		for j in [1..Size(iso_algebra)] do
			if internalAreAlgebrasIsomorphic (algebra_list[i],iso_algebra[j]) then
				iso := true;
				break;
			fi;			
		od;
		if iso = false then 
			Add(iso_algebra, algebra_list[i]);
		fi;
	od;

	return iso_algebra;

end;

#############################################################################
##
#F  internalAlgebrasFromModel( <axioms>, <dimension> )
##
##  Find all isomorphic algebras from axioms
##
internalAlgebrasFromModel := function (model_file, dimension)

	local dir, istream, ostream, algList, input, output, pos, algebra, i, j;
		
	dir := DirectoryCurrent();

	input := [];
	
	istream := InputTextFile(model_file);

	input := ReadAll (istream);
		
	algList:=[];
	
	pos := 1;
	repeat
	
		# Get next algebra model
	
		algebra := mace4GetAlgebraModel (input, pos, dimension);
		
		# Get subalgebra from ax model and congruence
		
		if not algebra[1] = fail then
			AddSet (algList,algebra[1]);
		fi;
		
		pos := algebra[2];
		
	until algebra[1] = fail or pos = fail;

	if Length (algList) > 1 then
		i := 1; 
		while i < Length (algList) do
			j := i + 1;
			while j <= Length (algList) do
				if internalAreAlgebrasIsomorphic (algList[i],algList[j]) then
					Remove (algList,j);
				else
					j := j+1;
				fi;
			od;
			i := i+1;
		od;
	fi;  
	
	return algList;

end;

#############################################################################
##
#F  internalAlgebrasFromAxioms( <axioms>, <dimension> )
##
##  Find all isomorphic algebras from axioms
##
internalAlgebrasFromAxioms := function (axioms, dimension)

	local dir, istream, ostream, algList, input, output, pos, algebra, i, j;
		
	dir := DirectoryCurrent();

	# Build mace4 input
		
	input := [];
	mace4Header (input, dimension,-1);
	mace4ClassAxioms(input, axioms);
	mace4Footer(input);
	
	istream := InputTextString( input );
	
	output := ""; 
	ostream := OutputTextString(output,true);
	
	# Run mace4 with input	
	
	Process( dir, CreamMace4Filename, istream, ostream, [] );
	
	# Get all algebras from output 	
	
	algList:=[];
	
	pos := 1;
	repeat
	
		# Get next algebra model
	
		algebra := mace4GetAlgebraModel (output, pos, dimension);
		
		# Get subalgebra from ax model and congruence
		
		if not algebra[1] = fail then
			AddSet (algList,algebra[1]);
		fi;
		
		pos := algebra[2];
		
	until algebra[1] = fail or pos = fail;

	if Length (algList) > 1 then
		i := 1; 
		while i < Length (algList) do
			j := i + 1;
			while j <= Length (algList) do
				if internalAreAlgebrasIsomorphic (algList[i],algList[j]) then
					Remove (algList,j);
				else
					j := j+1;
				fi;
			od;
			i := i+1;
		od;
	fi;  
	
	return algList;

end;

#############################################################################
##
#F  CreamAlgebrasFromAxioms( <axioms>, <dimension> )
##
##  Find all isomorphic algebras with a set of axioms
##
InstallGlobalFunction( CreamAlgebrasFromAxioms, function (axioms, dimension)

	return internalAlgebrasFromAxioms (axioms, dimension);

end );

#############################################################################
##
#F  getEndomorphismFromSubAlgebra( <salg>, <cong> )
##
##  Return the endomorphism mapping for a pair of congruence and sub-algebra.
##  The algorithm starts with the subalgebra encoded as an endomorphism with 
##  the elments not part of the subalgebra set to 0. The endomorphisms is 
##  calculated by setting the zeroed values considering that elements in the
##  same block must have the same mapping.
##  
##
getEndomorphismFromSubAlgebra := function (salg, cong)

	local dimension, i, j, irBlock, jrBlock, endo;	

	dimension := Size(salg);
	endo := ShallowCopy (salg);

	# For each element 
	for i in [1 .. dimension] do

		# If it is a subalgebra element (it isn't zeroed)  
		if not salg[i] = 0 then

			# Get the block  
			irBlock := cRootBlock (i, cong);
			for j in [1 .. dimension] do
				if endo[j] = 0 then
					jrBlock := cRootBlock (j, cong);
					
					# Any element in the same block...
					if irBlock = jrBlock then

						# ... will have the same mapping 
						endo[j] := salg[i];
					fi;
				fi;
			od;
		fi;
	od;	

	return endo;

end;

#############################################################################
##
#F  internalAllEndomorphismsNaive( <algebra> )
##
##  Return all endomorphism with prescribed kernel and image
##
internalAllEndomorphismsNaive := function (algebra)

	local s, subs, dir, istream, ostream, dimension, input, output, pos, fmodel, endos;
	
	dimension := internalSizeAlgebra(algebra);
		
	endos := [];
			
	dir := DirectoryCurrent();

	# Build mace4 input

	input := [];		
	mace4Header (input, dimension,-1);
	mace4AlgebraNamedOp(input, algebra, "");
	mace4Endomorphisms(input, algebra);
	mace4Footer(input);	
	
	istream := InputTextString( input );
	
	output := ""; 
	ostream := OutputTextString(output,true);
		
	# Run mace4 with input
	
	Process( dir, CreamMace4Filename, istream, ostream, [] );
		
	# Get all endomorphism model from output
	
	pos := 1;
	repeat
		
		# Get next f() model
		
		fmodel := mace4GetFModel (output, pos);
			
		# Get endomorphism from f() model and subalgebra and add endomorphism set
		
		if not fmodel[1] = fail then
			AddSet (endos, fmodel[1]);
		fi;
		
		pos := fmodel[2];
		
	until fmodel[1] = fail;
	
	# Return set of endomorphisms
	
	return endos;

end;

#############################################################################
##
#F  internalEndomorphismsFromCongruence( <algebra>, <cong> )
##
##  Find all subalgebras isomorphic to <algebra>/<cong>
##
internalEndomorphismsFromCongruence := function (algebra, cong, algebra_mace4)

	local dir, istream, ostream, dimension, subList, endoList, input, output, pos, amodel, sub, endo;
	
	dimension := internalSizeAlgebra(algebra);
		
	dir := DirectoryCurrent();

	# Build mace4 input
		
	input := [];
	if algebra_mace4 = [] then
		mace4Header (input, dimension,-1);
		mace4AlgebraNamedOp(input, algebra, "");
		Append (algebra_mace4, input);
	else
		Append (input, algebra_mace4);
	fi;
	mace4QuotientAlgebraAModel(input, algebra,cong);
	mace4Footer(input);
	
	istream := InputTextString( input );
	
	output := ""; 
	ostream := OutputTextString(output,true);
	
	# Run mace4 with input	
	
	Process( dir, CreamMace4Filename, istream, ostream, [] );
	
	# Get all subalgebras from output 	
	
	subList:=[];
	
	pos := 1;
	repeat
	
		# Get next ax model
	
		amodel := mace4GetAModel (output, pos, dimension);
		
		# Get subalgebra from ax model and congruence
		
		if not amodel[1] = fail then
			AddSet (subList,amodel[1]);
		fi;
		
		pos := amodel[2];
		
	until amodel[1] = fail or pos = fail;
	
	# Return the list of subalgebras

	endoList := [];

	for sub in subList do

		endo := getEndomorphismFromSubAlgebra (sub, cong); 
		AddSet (endoList,endo);

	od;
	
	return endoList;

end;


#############################################################################
##
#F  internalIsSingletonPartition( <cong> )
##
##  Test whether a partition is the singleton partition
##
internalIsSingletonPartition := function (cong)

	local isSingleton, elem;

	isSingleton := true;
	
	for elem in cong do
		
		if not elem = -1 then

			isSingleton := false;		
			break;
		fi;

	od;

	return isSingleton;

end;

#############################################################################
##
#F  internalIsEndomorphism( <algebra>, <endo> )
##
##  Return the congruence mapping for a pair of congruence and sub-algebra
##
internalIsEndomorphism := function (algebra, endo)

	local isEndo, func, dimension, i, j;

	isEndo := true;
	
	for func in algebra do

		dimension := CreamSizeAlgebra(algebra); 

		for i in [1 .. dimension] do

			if IsRectangularTable (func) then
			
				for j in [1 .. dimension] do
				
					if not endo[func[i,j]] = func[endo[i],endo[j]] then					
						isEndo := false;
						break;
					fi;

				od;

			else 

				if not endo[func[i]] = func[endo[i]] then
					isEndo := false;
					break;
				fi;
			fi;
					
			if not isEndo then
				break;
			fi;

		od;

		if not isEndo then
			break;
		fi;

	od;

	return isEndo;

end;


#############################################################################
##
#F  internalEndomorphisms( <algebra> )
##
##  Find all endomorphisms of algebra
##
internalEndomorphisms := function (algebra)

	local cong, congs, endos, endo_list, algebra_mace4;	

	if internalSizeAlgebra(algebra) < 60 then

		endo_list := internalAllEndomorphismsNaive (algebra);

	else
	
		endo_list := []; 
	
		# Get all algebra congruences	
	
		congs:= internalAllCongruences (algebra);

		algebra_mace4 := [];
		for cong in congs do
	
			# For each congruence
		
			# Get all endomorphisms with kernel cong

			if internalIsSingletonPartition (cong) then 

				endos := CreamAutomorphisms (algebra);

			else 

				endos := internalEndomorphismsFromCongruence (algebra, cong, algebra_mace4);

			fi;

			UniteSet (endo_list, endos);

		od;

	fi;

	return endo_list;

end;

#############################################################################
##
#F  internalAllEndomorphismsCongruences( <algebra> )
##
##  Find all endomorphisms of algebra
##
internalAllEndomorphismsCongruences := function (algebra)

	local cong, congs, endos, endo_list, algebra_mace4;	

	endo_list := []; 
	
	# Get all algebra congruences	
	
	congs:= internalAllCongruences (algebra);

	algebra_mace4 := [];
	for cong in congs do
	
		# For each congruence
		
		# Get all endomorphisms with kernel cong

		if internalIsSingletonPartition (cong) then 

			endos := CreamAutomorphisms (algebra);

		else 

			endos := internalEndomorphismsFromCongruence (algebra, cong, algebra_mace4);

		fi;

		UniteSet (endo_list, endos);

	od;

	return endo_list;

end;

#############################################################################
##
#F  CreamEndomorphisms( <multp> )
##
##  Find all endomorphisms of algebra
##
InstallGlobalFunction( CreamEndomorphisms, function (algebra)

	local dimension;
	
	dimension := CreamSizeAlgebra(algebra);
	
	if dimension = fail then
		return fail;
	else 
		return internalEndomorphisms (algebra);
	fi;

end );

#############################################################################
##
#F  internalPartitionFromType( <type> )
##
##	Create a partition from a list defining types of items.
##
##	Type is a list of items. For each two items type(i) and type (j), i and
##	j will be in the same block if type (i) = type (j).
##
internalPartitionFromType := function (type)

	local dimension, partition, i, j;
	
	dimension := Length (type);
	
	# Start with the sigleton partition
	
	partition := [1 .. dimension];
	for i in [1 .. dimension] do
		partition[i]:=-1;
	od;	
	
	for i in [1 .. dimension] do
		for j in [2 .. dimension] do
			if partition [i] < 1 and partition [j] < 1 then
			
				# if i and j are partition roots compare types (its only needed to compare partition roots)
			
				if type[i] = type[j] then
				
					# if types are equal join the blocks
				
					cJoinBlocks (i,j,partition);
				fi;
			fi;
		od;
	od;
	
	return partition;

end;

#############################################################################
##
#F  internalJoinPartition( <partition1>, <partition2> )
##
##  Joins partitions u and v. This means that 2 elements that belong to the
##  same block in either u or v will belong to the same block in the joined
##  partition. Also 2 elements that don't belong to the same block in both 
##  u and v will not belong to the same block in the joined partition.
##
internalJoinPartition := function (partition1, partition2)

	local partition;

	partition := cJoinPartition (partition1, partition2);
	
	return partition;

end;

#############################################################################
##
#F  internalMeetPartition( <partition1>, <partition2> )
##
##	Meet partition. Calculates the coarsest partition which is finer than 
##  both of them. x and y will be in be same block in the meet partition if 
##  they are in the same block in both partitions 
##
internalMeetPartition := function (partition1, partition2)

	local dimension, partition,i,j;
	
	dimension := Length (partition1);
	
	# Start with the sigleton partition
	
	partition := [1 .. dimension];
	for i in [1 .. dimension] do
		partition[i]:=-1;
	od;	
	
	for i in [1 .. dimension] do
		for j in [i+1 .. dimension] do
			if partition [i] < 1 and partition [j] < 1 then
			
				# If i and j are partition roots in the result partition then compare.
				# Its only needed to compare partition roots since non-roots are already 
				# explicitly or explicitly compared.
			
				if cRootBlock (i, partition1) = cRootBlock (j, partition1) and
					cRootBlock (i, partition2) = cRootBlock (j, partition2) then
					
					# if i and j are in the same block in both partitions then join their blocks in the result partition
					
					cJoinBlocks (i,j,partition);
				fi;
			fi;
		od;
	od;
	
	return partition;

end;

#############################################################################
##
#F  internalTrivialPartition( <dimension> )
##
##  Returns the trivial partition with dimension size
##
internalTrivialPartition := function (dimension)

	local partition,i;
	
	partition := [1 .. dimension];
	
	# Element 1 will be the root 
	
	partition[1] := -dimension;
	
	# Each other element will in block 1
	
	for i in [2 .. dimension] do
		partition[i]:=1;
	od;
	
	return partition;

end;

#############################################################################
##
#F  internalAllAutomorphismsMace4( <algebra> )
##
##  Return all automorphism from mace4
##
internalAllAutomorphismsMace4 := function (algebra)

	local s, subs, dir, istream, ostream, dimension, input, output, pos, fmodel, autos;
	
	dimension := internalSizeAlgebra(algebra);
		
	autos := [];
			
	dir := DirectoryCurrent();

	# Build mace4 input

	input := [];		
	mace4Header (input, dimension,-1);
	mace4AlgebraNamedOp(input, algebra, "");
	mace4MapAutomorphisms(input, algebra);
	mace4Footer(input);	
	
	istream := InputTextString( input );
	
	output := ""; 
	ostream := OutputTextString(output,true);
		
	# Run mace4 with input
	
	Process( dir, CreamMace4Filename, istream, ostream, [] );
		
	# Get all endomorphism model from output
	
	pos := 1;
	repeat
		
		# Get next f() model
		
		fmodel := mace4GetFModel (output, pos);
			
		# Get automorphism from f() model and subalgebra and add automorphism set
		
		if not fmodel[1] = fail then
			AddSet (autos, fmodel[1]);
		fi;
		
		pos := fmodel[2];
		
	until fmodel[1] = fail;
	
	# Return set of endomorphisms
	
	return autos;

end;

#############################################################################
##
#F  internalAutomorphisms( <algebra> )
##
##  Find algebra automorphisms using the Choiwah's functions
##
internalAutomorphisms := function (algebra)

	local dimension, autos, auto_grp;
	
	dimension := internalSizeAlgebra(algebra);	

	auto_grp := Algebra_AutomorphismGroup(algebra);

	if auto_grp = fail then 
		autos := internalAllAutomorphismsMace4(algebra);
	else	
		autos := List (AsSet(auto_grp), x->ListPerm(x,dimension));
	fi;
	
	return autos;

end;

#############################################################################
##
#F  CreamAutomorphisms( <algebra> )
##
##  Find algebra automorphisms using the Choiwah's functions 
##
InstallGlobalFunction( CreamAutomorphisms, function (algebra)

	local dimension, cong;
	
	dimension := CreamSizeAlgebra(algebra);
	
	if dimension = fail then
		return fail;
	else 
		return internalAutomorphisms (algebra);
	fi;

end );

#############################################################################
##
#F  internalSizeSubAlgebra ([<salgebra>])   
##
##  internalSizeSubAlgebra returns the sub-algebra order and the minimum 
##  possible size of the parent algebra. For determining the order, it also
##  determines whether the sub-algebra is valid. A sub-algebra will be 
##  valid if the vector doesn't have repeated elements and the elements 
##  are.
##
internalSizeSubAlgebra := function( salgebra )

	local element, last_element;
	
	if not IsList (salgebra) then
		return fail;
	fi;

	last_element := 0;	
	for element in salgebra do 
		if element <= last_element then
			return fail;
		fi;
		last_element := element;
	od;

	return [Length(salgebra),last_element];

end;

#############################################################################
##
#F  internalSubAlgebraFromCongruence( <algebra> , <cong> )
##
##  Return subalgebra from an algebra and a congruence
##
internalSubAlgebraFromCongruence := function (algebra, cong)

	local dimension, dimension2, current_block, map, maptos, op, nops, salgebra, i, j;

	dimension := internalSizeAlgebra (algebra);
	dimension2 := CreamNumberOfBlocks (cong);
	nops := Length (algebra);

	map:=[];
	maptos:=[];
	current_block:=1;
	for i in [1 .. dimension] do
		if cong[i] < 0 then 
			map[current_block] := i;
			maptos[i]:= current_block;
			current_block := current_block + 1;
		else
			maptos[i]:= maptos[cRootBlock(i,cong)];
		fi;
	od;

	salgebra := [];
	for op in [1 .. nops] do
		if IsRectangularTable (algebra[op]) then
			salgebra[op] := [];
			for i in [1 .. dimension2] do
				salgebra[op][i] := []; 
				for j in [1 .. dimension2] do
					salgebra[op][i][j] := algebra[op][map[i]][map[j]];
				od;
			od;
		else
			salgebra[op] := [];
			for i in [1 .. dimension2] do 
				salgebra[op][i] := algebra[op][map[i]];
			od;			
		fi;		
	od;

	return [salgebra,maptos];

end;

#############################################################################
##
#F  internalSubUniverse2Algebra ( <algebra>, <salgebra> )
##
##  Returns the algebra corresponding a subalgebra represented by its
##  subuniverse
##
internalSubUniverse2Algebra := function (algebra, salgebra)

	local nops, dimension, subDimension, saalgebra, i, j, k;	
	
	nops := Length (algebra);
	dimension := Length (algebra[1]);
	subDimension := Length (salgebra);
	
	saalgebra := [];
	for i in [1 .. nops] do
		if IsRectangularTable (algebra[i]) then
		
			# Binary function
			
			saalgebra[i] := [];
			for j in [1 .. subDimension] do
				saalgebra[i][j] := [];
				for k in [1 .. subDimension] do
					saalgebra[i][j][k] := Position(salgebra, algebra[i][salgebra[j]][salgebra[k]]);
				od;
			od;		
		else
		
			# Unary function

			saalgebra[i] := [];			
			for j in [1 .. subDimension] do
				saalgebra[i][j] := Position(salgebra, algebra[i][salgebra[j]]);				
			od;
		fi;
	od;

	return saalgebra;

end;

#############################################################################
##
#F  CreamSubUniverse2Algebra( <algebra> , <salgebra>  )
##
##  Returns the algebra corresponding a subalgebra represented by its
##  subuniverse 
##
InstallGlobalFunction( CreamSubUniverse2Algebra, function (algebra, salgebra)

	local dimension, dimension2;
	
	dimension := CreamSizeAlgebra(algebra);
	if dimension = fail then
		return fail;
	fi;

	dimension2 := internalSizeSubAlgebra (salgebra);
	if dimension2 = fail then
		return fail;
	fi;

	if dimension < dimension2[1] then
		return fail;
	fi;

	if dimension < dimension2[2] then
		return fail;
	fi;	
 
	return internalSubUniverse2Algebra (algebra, salgebra);

end );

#############################################################################
##
#F  internalSubAlgebraFromElement( <algebra>, <salgebra>, <element> )
##
##  Returns the subalgebra derived from an element
##
internalSubAlgebraFromElement := function (algebra, isalgebra, element)

	local dimension, i, salgebra, elements, nelem, current_elem, op, new_elem, new_elems, sa_elem;	
	
	dimension := internalSizeAlgebra(algebra);

	elements:= [];
	new_elems := [];

	if isalgebra = [] then
		salgebra := [element];
		for op in algebra do  
			if IsRectangularTable (op) then
				AddSet (new_elems, op[element][element]);
			else
				AddSet (new_elems, op[element]);
			fi;
		od;

		for new_elem in new_elems do
			if new_elem <> element then	
				AddSet (elements, new_elem);
			fi;
		od;		

	else
		salgebra := [];
		for current_elem in isalgebra do
			AddSet (salgebra, current_elem);
		od;
		elements:= [element];
	fi;

	nelem := Length (elements);

	while nelem > 0 do

		current_elem := elements[nelem];
		Remove (elements);

		new_elems := [];
		for op in algebra do  
			if IsRectangularTable (op) then
				AddSet (new_elems, op[current_elem][current_elem]);
				for sa_elem in salgebra do
					AddSet (new_elems, op[current_elem][sa_elem]);
					AddSet (new_elems, op[sa_elem][current_elem]);
				od;
			else
				AddSet (new_elems, op[current_elem]);
			fi;
		od;

		AddSet (salgebra, current_elem);

		for new_elem in new_elems do
			if not new_elem in salgebra then	
				AddSet (elements, new_elem);
			fi;
		od;

		nelem := Length (elements);

		if Length (salgebra) + nelem = dimension then
			salgebra := [1 .. dimension];
			nelem := 0;
		fi;

	od;

	return salgebra;

end;


#############################################################################
##
#F  internalAutomorphicElements ( <aut>, <elist> )
##
##  Returns the elements automorphic to the elements in elist
##
internalAutomorphicElements := function (autos, elist)

	local element, elements, aut;	

	elements := [];
	for element in elist do
		for aut in autos do
			AddSet (elements, aut[element]);
		od;
	od;

	return elements;

end;

#############################################################################
##
#F  internalAutomorphicSubAlgebras ( <aut>, <salgebra> )
##
##  Returns the subalgebras automorphic to subalgebra
##
internalAutomorphicSubAlgebras := function (autos, salgebra)

	local salgebras, asalgebra, aut, i;	

	salgebras := [];
	for aut in autos do
		asalgebra := [];
		for i in salgebra do
			AddSet (asalgebra, aut[i]);
		od;
		AddSet (salgebras, asalgebra);
	od;

	return salgebras;

end;

#############################################################################
##
#F  internalAllSubUniverses( <algebra> )
##
##  This function returns the subuniverses of the subalgebras of A
##
internalAllSubUniverses := function (algebra)

	local salgebra, salgebras, autos, autoes, sigma, sigma_expanded, sigmas, sigmaminus, csigma, onesigma, dimension, elements, i, j;	
	
	dimension := internalSizeAlgebra(algebra);

	autos := internalAutomorphisms (algebra);

	sigma:=[];
	sigma_expanded:=[];
	sigma[1]:=[];
	elements := [1 .. dimension];
	while Length (elements) <> 0 do
		j := elements [1]; 
		salgebra := internalSubAlgebraFromElement (algebra, [], j);
		AddSet(sigma[1], salgebra);	
		autoes := internalAutomorphicElements (autos, [j]);
		SubtractSet (elements, autoes);
	od;

	sigma_expanded[1]:=[];
	for salgebra in sigma[1] do
		UniteSet (sigma_expanded[1], internalAutomorphicSubAlgebras (autos, salgebra));
	od;	

	for i in [2 .. dimension] do
		sigma[i]:=[];
		sigmaminus := sigma_expanded[i-1];
		for csigma in sigmaminus do
			elements := [1 .. dimension];
			SubtractSet (elements, csigma);
			
			while Length (elements) <> 0 do
				j := elements [1]; 
				salgebra := internalSubAlgebraFromElement (algebra, csigma, j);
				AddSet(sigma[i], salgebra);
				autoes := internalAutomorphicElements (autos, [j]);
				SubtractSet (elements, autoes); 
			od;
				
		od;
		sigma_expanded[i]:=[];
		for salgebra in sigma[i] do
			UniteSet (sigma_expanded[i], internalAutomorphicSubAlgebras (autos, salgebra));
		od;
		if Length(sigma[i]) = 1 and Length(sigma[i][1]) = dimension then
			break;
		fi;
	od;

	salgebras := [];

	for csigma in sigma_expanded do
		UniteSet (salgebras, csigma);
	od;

	return salgebras;

end;


#############################################################################
##
#F  CreamAllSubUniverses( <algebra> )
##
##  This function returns the subuniverses of the subalgebras of A
##
InstallGlobalFunction( CreamAllSubUniverses, function (algebra)

	local dimension;
	
	dimension := CreamSizeAlgebra(algebra);
	if dimension = fail then
		return fail;
	fi;
 
	return internalAllSubUniverses (algebra);

end );

#############################################################################
##
#F  internalExistsEpimorphism( <algebra1> , <algebra2> )
##
##  Find whether there is an epimorphism between 2 algebras
##
internalExistsEpimorphism := function (algebra1, algebra2)

	local cong, congs, dimension2, salgebra, map;	
	
	# Get all algebra congruences	
	
	congs:= internalAllCongruences (algebra1);
	dimension2 := internalSizeAlgebra (algebra2);

	for cong in congs do
	
		# For each congruence
		
		# Get check the congruences with a number of blocks similar algebra2 size

		if dimension2 = CreamNumberOfBlocks (cong) then 

			salgebra := internalSubAlgebraFromCongruence (algebra1, cong);

			if internalAreAlgebrasIsomorphic (algebra2, salgebra[1]) then
				return true;
			fi;

		fi;
	od;

	return false;

end;

#############################################################################
##
#F  CreamExistsEpimorphism( <algebra1> , <algebra2> )
##
##  Find whether there is an epimorphism between 2 algebras
##
InstallGlobalFunction( CreamExistsEpimorphism, function (algebra1, algebra2)

	local dimension, nops1, nops2, op;
	
	dimension := CreamSizeAlgebra(algebra1);
	if dimension = fail then
		return fail;
	fi;

	dimension := CreamSizeAlgebra(algebra2);
	if dimension = fail then
		return fail;
	fi;

	nops1 := Length (algebra1);
	nops2 := Length (algebra2);
	if not nops1 = nops2 then
		return fail;
	fi;

	for op in [1 .. nops1] do
		if IsRectangularTable (algebra1[op]) then
			if not IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		else
			if IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		fi;
	od;
 
	return internalExistsEpimorphism (algebra1, algebra2);

end );

#############################################################################
##
#F  internalOneEpimorphism( <algebra1> , <algebra2> )
##
##  Find one epimorphism between 2 algebras
##
internalOneEpimorphism := function (algebra1, algebra2)

	local cong, congs, dimension1, dimension2, salgebra, map, iso_perm, epi, i;	
	
	# Get all algebra congruences	
	
	congs:= internalAllCongruences (algebra1);
	dimension1 := internalSizeAlgebra (algebra1);
	dimension2 := internalSizeAlgebra (algebra2);

	for cong in congs do
	
		# For each congruence
		
		# Get check the congruences with a number of blocks similar algebra2 size

		if dimension2 = CreamNumberOfBlocks (cong) then 

			salgebra := internalSubAlgebraFromCongruence (algebra1, cong);
			iso_perm := IsomorphismAlgebras (salgebra[1],algebra2);

			if not iso_perm = fail then
				iso_perm := ListPerm(iso_perm, dimension2); 
				epi:=[];
				for i in [1 .. dimension1] do
					epi[i]:=iso_perm[salgebra[2][i]];
				od;
				return epi;
			fi;

		fi;
	od;

	return fail;

end;

#############################################################################
##
#F  CreamOneEpimorphism( <algebra1> , <algebra2> )
##
##  Find one epimorphism between 2 algebras
##
InstallGlobalFunction( CreamOneEpimorphism, function (algebra1, algebra2)

	local dimension, nops1, nops2, op;
	
	dimension := CreamSizeAlgebra(algebra1);
	if dimension = fail then
		return fail;
	fi;

	dimension := CreamSizeAlgebra(algebra2);
	if dimension = fail then
		return fail;
	fi;

	nops1 := Length (algebra1);
	nops2 := Length (algebra2);
	if not nops1 = nops2 then
		return fail;
	fi;

	for op in [1 .. nops1] do
		if IsRectangularTable (algebra1[op]) then
			if not IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		else
			if IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		fi;
	od;
 
	return internalOneEpimorphism (algebra1, algebra2);

end );

#############################################################################
##
#F  internalAllEpimorphism( <algebra1> , <algebra2> )
##
##  Find the epimorphisms between 2 algebras
##
internalAllEpimorphisms := function (algebra1, algebra2)

	local cong, congs, dimension1, dimension2, salgebra, iso_perm, auto, autos, epi, epis, i;	
	
	# Get all algebra congruences	
	
	congs:= internalAllCongruences (algebra1);
	dimension1 := internalSizeAlgebra (algebra1);
	dimension2 := internalSizeAlgebra (algebra2);
	autos := [];

	#algebra_mace4 := [];
	epis := [];
	for cong in congs do
	
		# For each congruence
		
		# Get check the congruences with a number of blocks similar algebra2 size

		if dimension2 = CreamNumberOfBlocks (cong) then 

			salgebra := internalSubAlgebraFromCongruence (algebra1, cong);
			iso_perm := IsomorphismAlgebras (salgebra[1],algebra2);

			if not iso_perm = fail then
				iso_perm := ListPerm(iso_perm, dimension2);
				if autos = [] then 
					autos := internalAutomorphisms (algebra2);
				fi;
				for auto in autos do
					epi:=[];
					for i in [1 .. dimension1] do
						epi[i]:=auto[iso_perm[salgebra[2][i]]];
					od;
					AddSet(epis,epi);
				od;
			fi;

		fi;
	od;

	return epis;

end;

#############################################################################
##
#F  CreamAllEpimorphisms( <algebra1> , <algebra2> )
##
##  Find the is an epimorphisms between 2 algebras
##
InstallGlobalFunction( CreamAllEpimorphisms, function (algebra1, algebra2)

	local dimension, nops1, nops2, op;
	
	dimension := CreamSizeAlgebra(algebra1);
	if dimension = fail then
		return fail;
	fi;

	dimension := CreamSizeAlgebra(algebra2);
	if dimension = fail then
		return fail;
	fi;

	nops1 := Length (algebra1);
	nops2 := Length (algebra2);
	if not nops1 = nops2 then
		return fail;
	fi;

	for op in [1 .. nops1] do
		if IsRectangularTable (algebra1[op]) then
			if not IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		else
			if IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		fi;
	od;
 
	return internalAllEpimorphisms (algebra1, algebra2);

end );

#############################################################################
##
#F  internalExistsMonomorphism( <algebra1> , <algebra2> )
##
##  Find whether there is a monomorphism between 2 algebras
##
internalExistsMonomorphism := function (algebra1, algebra2)

	return not CreamMonomorphismAlgebras(algebra1, algebra2) = fail;

end;

#############################################################################
##
#F  CreamExistsMonomorphism( <algebra1> , <algebra2> )
##
##  Find whether there is a monomorphism between 2 algebras
##
InstallGlobalFunction( CreamExistsMonomorphism, function (algebra1, algebra2)

	local dimension, nops1, nops2, op;
	
	dimension := CreamSizeAlgebra(algebra1);
	if dimension = fail then
		return fail;
	fi;

	dimension := CreamSizeAlgebra(algebra2);
	if dimension = fail then
		return fail;
	fi;

	nops1 := Length (algebra1);
	nops2 := Length (algebra2);
	if not nops1 = nops2 then
		return fail;
	fi;

	for op in [1 .. nops1] do
		if IsRectangularTable (algebra1[op]) then
			if not IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		else
			if IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		fi;
	od;
 
	return internalExistsMonomorphism (algebra1, algebra2);

end );

#############################################################################
##
#F  internalOneMonomorphism( <algebra1> , <algebra2> )
##
##  Find a monomorphism between 2 algebras
##
internalOneMonomorphism := function (algebra1, algebra2)

	return CreamMonomorphismAlgebras(algebra1, algebra2);

end;

#############################################################################
##
#F  CreamOneMonomorphism( <algebra1> , <algebra2> )
##
##  Find a monomorphism between 2 algebras
##
InstallGlobalFunction( CreamOneMonomorphism, function (algebra1, algebra2)

	local dimension, nops1, nops2, op;
	
	dimension := CreamSizeAlgebra(algebra1);
	if dimension = fail then
		return fail;
	fi;

	dimension := CreamSizeAlgebra(algebra2);
	if dimension = fail then
		return fail;
	fi;

	nops1 := Length (algebra1);
	nops2 := Length (algebra2);
	if not nops1 = nops2 then
		return fail;
	fi;

	for op in [1 .. nops1] do
		if IsRectangularTable (algebra1[op]) then
			if not IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		else
			if IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		fi;
	od;
 
	return internalOneMonomorphism (algebra1, algebra2);

end );

#############################################################################
##
#F  internalAllMonomorphisms( <algebra1> , <algebra2> )
##
##  Find the monomorphisms between 2 algebras
##
internalAllMonomorphisms := function (algebra1, algebra2)

	return CreamAllMonomorphismAlgebras(algebra1, algebra2);

end;

#############################################################################
##
#F  CreamAllMonomorphisms( <algebra1> , <algebra2> )
##
##  Find the monomorphisms between 2 algebras
##
InstallGlobalFunction( CreamAllMonomorphisms, function (algebra1, algebra2)

	local dimension, nops1, nops2, op;
	
	dimension := CreamSizeAlgebra(algebra1);
	if dimension = fail then
		return fail;
	fi;

	dimension := CreamSizeAlgebra(algebra2);
	if dimension = fail then
		return fail;
	fi;

	nops1 := Length (algebra1);
	nops2 := Length (algebra2);
	if not nops1 = nops2 then
		return fail;
	fi;

	for op in [1 .. nops1] do
		if IsRectangularTable (algebra1[op]) then
			if not IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		else
			if IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		fi;
	od;
 
	return internalAllMonomorphisms (algebra1, algebra2);

end );

#############################################################################
##
#F  internalAllPartialIsomorphisms( <algebra> )
##
##  Find the partial isomorphisms of an algebra
##
internalAllPartialIsomorphisms := function (algebra)

	local subU, subA, nSubU, Autos, saIdx, saIdx2, isEqual, sAlg, PIsos, autIdx, pt, elemIdx, posElem, monoMorph, nMono, monoIdx; 

	subU := internalAllSubUniverses (algebra);
	subA := List( subU, u->internalSubUniverse2Algebra(algebra,u));
	
	nSubU := Size(subU);
	
	Autos := [];
	Add (Autos, internalAutomorphisms (subA[1]));
	
	for saIdx in [2 .. nSubU] do
		isEqual := false;
		for saIdx2 in [1 .. saIdx-1] do
			if subA[saIdx] = subA[saIdx2] then
				isEqual := true;
				break;
			fi;
		od;
		if isEqual then
			Add (Autos, Autos[saIdx2]);
		else
			Add (Autos, internalAutomorphisms (subA[saIdx]));
		fi;
	od;
	
	sAlg := Size(algebra[1]);
	PIsos := [];
	for saIdx in [1 .. nSubU] do
		for autIdx in [1 .. Size (Autos[saIdx])] do
			pt := ListWithIdenticalEntries (sAlg,0);
			for elemIdx in [1 .. sAlg] do
				posElem := Position (subU[saIdx], elemIdx);
				if not posElem = fail then
					pt[elemIdx] := subU[saIdx][Autos[saIdx][autIdx][posElem]];
				fi;
			od;
			AddSet (PIsos, pt);
		od;
	od;
	
	for saIdx in [1 .. nSubU] do
		for saIdx2 in [1 .. nSubU] do
			if not (saIdx = saIdx2) and Intersection(subU[saIdx],subU[saIdx2])=[] then
					pt := ListWithIdenticalEntries (sAlg,0);
					AddSet (PIsos, pt);
			fi;
			if not (saIdx = saIdx2) and internalAreAlgebrasIsomorphic(subA[saIdx],subA[saIdx2]) then 
				monoMorph := internalAllMonomorphisms (subA[saIdx],subA[saIdx2]);
				nMono := Size(monoMorph);
				for monoIdx in [1 .. nMono] do
					pt := ListWithIdenticalEntries (sAlg,0);
					for elemIdx in [1 .. sAlg] do
						posElem := Position (subU[saIdx], elemIdx);
						if not posElem = fail then
							pt[elemIdx] := subU[saIdx2][monoMorph[monoIdx][posElem]];
						fi;
					od;
					AddSet (PIsos, pt);					
				od;
			fi;
		od;
	od;
	
	return PIsos;
end;

#############################################################################
##
#F  CreamAllPartialIsomorphisms( <algebra> )
##
##  Find the partial isomorphisms of an algebra
##
InstallGlobalFunction( CreamAllPartialIsomorphisms, function (algebra)

	local dimension;
	
	dimension := CreamSizeAlgebra(algebra);
	if dimension = fail then
		return fail;
	fi;
 
	return internalAllPartialIsomorphisms (algebra);

end );


#############################################################################
##
#F  internalExistsDivisor( <algebra1> , <algebra2> )
##
##  Find whether there is a divisor between 2 algebras
##
internalExistsDivisor := function (algebra1, algebra2)

	local cong, congs, rcongs, dimension, salgebra, salgebras, saalgebra, cralgebra, craalgebra, divisors;	
	
	# Get all sub-algebras
	
	dimension := internalSizeAlgebra (algebra2);

	salgebras := internalAllSubUniverses (algebra1);

	divisors := [];
	for salgebra in salgebras do
	
		# For each subalgebra
		
		# Check the subalgebras with the same or larger size of the algebra2

		if dimension <= Length (salgebra) then

			saalgebra := internalSubUniverse2Algebra (algebra1, salgebra);
	
			congs:= internalAllCongruences (saalgebra);

			rcongs := [];
			for cong in congs do
	
				# For each congruence
		
				# Get the congruences with a number of blocks equal to algebra2 size

				if dimension = CreamNumberOfBlocks (cong) then 
					AddSet (rcongs, cong);
				fi;
			od;

			for cong in rcongs do

				cralgebra := internalSubAlgebraFromCongruence (saalgebra, cong);

				if internalAreAlgebrasIsomorphic (cralgebra[1], algebra2) then
					return true;
				fi;
			od;

		fi;
	od;

	return false;

end;

#############################################################################
##
#F  CreamExistsDivisor( <algebra1> , <algebra2> )
##
##  Find whether there is a divisor between 2 algebras
##
InstallGlobalFunction( CreamExistsDivisor, function (algebra1, algebra2)

	local dimension, nops1, nops2, op;
	
	dimension := CreamSizeAlgebra(algebra1);
	if dimension = fail then
		return fail;
	fi;

	dimension := CreamSizeAlgebra(algebra2);
	if dimension = fail then
		return fail;
	fi;

	nops1 := Length (algebra1);
	nops2 := Length (algebra2);
	if not nops1 = nops2 then
		return fail;
	fi;

	for op in [1 .. nops1] do
		if IsRectangularTable (algebra1[op]) then
			if not IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		else
			if IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		fi;
	od;
 
	return internalExistsDivisor (algebra1, algebra2);

end );

#############################################################################
##
#F  internalOneDivisorUniverse( <algebra1> , <algebra2> )
##
##  Find a divisor between 2 algebras
##
internalOneDivisorUniverse := function (algebra1, algebra2)

	local cong, congs, rcongs, dimension, salgebra, salgebras, saalgebra, cralgebra, craalgebra, divisors;	
	
	# Get all sub-algebras
	
	dimension := internalSizeAlgebra (algebra2);

	salgebras := internalAllSubUniverses (algebra1);

	divisors := [];
	for salgebra in salgebras do
	
		# For each subalgebra
		
		# Check the subalgebras with the same or larger size of the algebra2

		if dimension <= Length (salgebra) then

			saalgebra := internalSubUniverse2Algebra (algebra1, salgebra);
	
			congs:= internalAllCongruences (saalgebra);

			rcongs := [];
			for cong in congs do
	
				# For each congruence
		
				# Get the congruences with a number of blocks equal to algebra2 size

				if dimension = CreamNumberOfBlocks (cong) then 
					AddSet (rcongs, cong);
				fi;
			od;

			for cong in rcongs do

				cralgebra := internalSubAlgebraFromCongruence (saalgebra, cong);

				if internalAreAlgebrasIsomorphic (cralgebra[1], algebra2) then
					return [salgebra, cong];
				fi;
			od;

		fi;
	od;

	return fail;

end;

#############################################################################
##
#F  CreamOneDivisorUniverse( <algebra1> , <algebra2> )
##
##  Find a divisor between 2 algebras
##
InstallGlobalFunction( CreamOneDivisorUniverse, function (algebra1, algebra2)

	local dimension, nops1, nops2, op;
	
	dimension := CreamSizeAlgebra(algebra1);
	if dimension = fail then
		return fail;
	fi;

	dimension := CreamSizeAlgebra(algebra2);
	if dimension = fail then
		return fail;
	fi;

	nops1 := Length (algebra1);
	nops2 := Length (algebra2);
	if not nops1 = nops2 then
		return fail;
	fi;

	for op in [1 .. nops1] do
		if IsRectangularTable (algebra1[op]) then
			if not IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		else
			if IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		fi;
	od;
 
	return internalOneDivisorUniverse (algebra1, algebra2);

end );

#############################################################################
##
#F  internalDivisorUniverses( <algebra1> , <algebra2> )
##
##  Find the divisors between 2 algebras returning their universes
##  The corresponding algebras can be obtain using CreamSubAlgebra2Algebra
##
internalAllDivisorUniverses := function (algebra1, algebra2)

	local cong, congs, rcongs, dimension, salgebra, salgebras, saalgebra, cralgebra, craalgebra, divisors;	
	
	# Get all sub-algebras
	
	dimension := internalSizeAlgebra (algebra2);

	salgebras := internalAllSubUniverses (algebra1);

	divisors := [];
	for salgebra in salgebras do
	
		# For each subalgebra
		
		# Check the subalgebras with the same or larger size of the algebra2

		if dimension <= Length (salgebra) then

			saalgebra := internalSubUniverse2Algebra (algebra1, salgebra);
	
			congs:= internalAllCongruences (saalgebra);

			rcongs := [];
			for cong in congs do
	
				# For each congruence
		
				# Get the congruences with a number of blocks equal to algebra2 size

				if dimension = CreamNumberOfBlocks (cong) then 
					AddSet (rcongs, cong);
				fi;
			od;

			for cong in rcongs do

				cralgebra := internalSubAlgebraFromCongruence (saalgebra, cong);

				if internalAreAlgebrasIsomorphic (cralgebra[1], algebra2) then
					AddSet (divisors, [salgebra, cong]);
				fi;
			od;

		fi;
	od;

	return divisors;

end;

#############################################################################
##
#F  CreamAllDivisorUniverses( <algebra1> , <algebra2> )
##
##  Find the divisors between 2 algebras
##
InstallGlobalFunction( CreamAllDivisorUniverses, function (algebra1, algebra2)

	local dimension, nops1, nops2, op;
	
	dimension := CreamSizeAlgebra(algebra1);
	if dimension = fail then
		return fail;
	fi;

	dimension := CreamSizeAlgebra(algebra2);
	if dimension = fail then
		return fail;
	fi;

	nops1 := Length (algebra1);
	nops2 := Length (algebra2);
	if not nops1 = nops2 then
		return fail;
	fi;

	for op in [1 .. nops1] do
		if IsRectangularTable (algebra1[op]) then
			if not IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		else
			if IsRectangularTable (algebra2[op]) then
				return fail;
			fi;
		fi;
	od;  
 
	return internalAllDivisorUniverses (algebra1, algebra2);

end );

#############################################################################
##
#F  internalExistsDirectFactorization( <algebra> )
##
##  For algebra A there are commuting congruences R, T where A/R is 
##  isomorphic to A/T
##
internalExistsDirectFactorization := function (algebra)

	local cong, congs, pCongs, dimension, congNBlocks, pCongsNBlocks, pairs, nCongs, i, j, cMeet, cMeetNBlocks;	
	
	# Get all algebra congruences	
	
	dimension := internalSizeAlgebra (algebra);

	congs:= internalAllCongruences (algebra);

	pCongs := [];
	pCongsNBlocks := [];
	for cong in congs do 
		congNBlocks := CreamNumberOfBlocks (cong);
		if congNBlocks <> 1 and congNBlocks <> dimension and IsInt(dimension/congNBlocks) then
			Add (pCongs, cong);
			Add (pCongsNBlocks, congNBlocks);
		fi;
	od;

	pairs := [];
	nCongs := Length (pCongs);
	for i in [1 .. nCongs] do 
		for j in [i .. nCongs] do
			if pCongsNBlocks[i]*pCongsNBlocks[j] = dimension then
				cMeet := internalMeetPartition (pCongs[i], pCongs[j]);
				cMeetNBlocks := CreamNumberOfBlocks (cMeet);
				if cMeetNBlocks = dimension then
					return true; 
				fi;
			fi;
		od;
	od;

	return false;

end;


#############################################################################
##
#F  CreamExistsDirectFactorization( <algebra> )
##
##  For algebra A there are commuting congruences R, T where A/R is 
##  isomorphic to A/T
##
InstallGlobalFunction( CreamExistsDirectFactorization, function (algebra)

	local dimension;
	
	dimension := CreamSizeAlgebra(algebra);
	if dimension = fail then
		return fail;
	fi;
 
	return internalExistsDirectFactorization (algebra);

end );

#############################################################################
##
#F  internalOneDirectFactorization( <algebra> )
##
##  For algebra A there is a commuting congruence R, T where A/R is 
##  isomorphic to A/T
##
internalOneDirectFactorization := function (algebra)

	local cong, congs, pCongs, dimension, congNBlocks, pCongsNBlocks, pairs, nCongs, i, j, cMeet, cMeetNBlocks;	
	
	# Get all algebra congruences	
	
	dimension := internalSizeAlgebra (algebra);

	congs:= internalAllCongruences (algebra);

	pCongs := [];
	pCongsNBlocks := [];
	for cong in congs do 
		congNBlocks := CreamNumberOfBlocks (cong);
		if congNBlocks <> 1 and congNBlocks <> dimension and IsInt(dimension/congNBlocks) then
			Add (pCongs, cong);
			Add (pCongsNBlocks, congNBlocks);
		fi;
	od;

	pairs := [];
	nCongs := Length (pCongs);
	for i in [1 .. nCongs] do 
		for j in [i .. nCongs] do
			if pCongsNBlocks[i]*pCongsNBlocks[j] = dimension then
				cMeet := internalMeetPartition (pCongs[i], pCongs[j]);
				cMeetNBlocks := CreamNumberOfBlocks (cMeet);
				if cMeetNBlocks = dimension then
					return [pCongs[i],pCongs[j]]; 
				fi;
			fi;
		od;
	od;

	return fail;

end;


#############################################################################
##
#F  CreamOneDirectFactorization( <algebra> )
##
##  For algebra A there is a commuting congruence R, T where A/R is 
##  isomorphic to A/T
##
InstallGlobalFunction( CreamOneDirectFactorization, function (algebra)

	local dimension;
	
	dimension := CreamSizeAlgebra(algebra);
	if dimension = fail then
		return fail;
	fi;
 
	return internalOneDirectFactorization (algebra);

end );

#############################################################################
##
#F  internalAllDirectFactors( <algebra> )
##
##  For algebra A find commuting congruences R, T where A/R is isomorphic to
##  A/T
##
internalAllDirectFactors := function (algebra)

	local cong, congs, pCongs, dimension, congNBlocks, pCongsNBlocks, pairs, nCongs, i, j, cMeet, cMeetNBlocks;	
	
	# Get all algebra congruences	
	
	dimension := internalSizeAlgebra (algebra);

	congs:= internalAllCongruences (algebra);

	pCongs := [];
	pCongsNBlocks := [];
	for cong in congs do 
		congNBlocks := CreamNumberOfBlocks (cong);
		if congNBlocks <> 1 and congNBlocks <> dimension and IsInt(dimension/congNBlocks) then
			Add (pCongs, cong);
			Add (pCongsNBlocks, congNBlocks);
		fi;
	od;

	pairs := [];
	nCongs := Length (pCongs);
	for i in [1 .. nCongs] do 
		for j in [i+1 .. nCongs] do
			if pCongsNBlocks[i]*pCongsNBlocks[j] = dimension then
				cMeet := internalMeetPartition (pCongs[i], pCongs[j]);
				cMeetNBlocks := CreamNumberOfBlocks (cMeet);
				if cMeetNBlocks = dimension then
					Add (pairs, [pCongs[i],pCongs[j]]); 
				fi;
			fi;
		od;
	od;

	return pairs;

end;


#############################################################################
##
#F  CreamAllDirectFactors( <algebra> )
##
##  For algebra A find commuting congruences R, T where A/R is isomorphic to
##  A/T
##
InstallGlobalFunction( CreamAllDirectFactors, function (algebra)

	local dimension;
	
	dimension := CreamSizeAlgebra(algebra);
	if dimension = fail then
		return fail;
	fi;
 
	return internalAllDirectFactors (algebra);

end );

#E  files.gi  . . . . . . . . . . . . . . . . . . . . . . . . . . . ends here

