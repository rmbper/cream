#############################################################################
##
#W  Cream_iso.gi  Monomorphism for Magmas and Algebras in Cream
##  
##
#Y  Copyright (C)  2021,  João Araújo (Universidade Nova de Lisboa, Portugal),
#Y                        Choiwah Chow (Universidade Aberta, Portugal)
##  
##


############################################################################
##  Invariants
##  -------------------------------------------------------------------------


InstallGlobalFunction(CREAM_CompatibleInvariants,
function(InvL, InvM)
	return InvL <= InvM;
end);


############################################################################
##  MONOMORPHISMS
##  -------------------------------------------------------------------------


#############################################################################
##  
#F  CREAM_ExtendAllMonomorphisms( f, L, GenL, M, RangeM, findAll )  
##  
##  Auxiliary function.
##  Inputs:
##		f:  a partial map from L to M
##	    L:  multiplication table of a magma (domain)
##      GenL : partial efficient generators of L - only generators not already used are here
##      M    : multiplication table of a magma (range)
##      RangeM : list of possible choices for each elements of L (equal invariants)
##      findAll: true if all monomorphisms are to be returned, otherwise, just one
##  Returns: an injective homomorphism from L to M, fail if it cannot find one.
##  This function attempts to extend f into an injective homomorphism from L to M.

InstallGlobalFunction(CREAM_ExtendAllMonomorphisms,
function(f, L, GenL, M, RangeM, findAll)
    local x, possible_images, y, g, r, all_mono;
    
    all_mono := [];
    if Length(GenL) = 0 then
    	f := MAGMAS_ExtendHomomorphismByClosingSource( f, L, M );
    	if (not f = fail) and Length( f[ 2 ] ) = Length( L ) and IsDuplicateFree(f[1]) then return [f[1]]; fi;
    	return [];
    fi;

    x := GenL[ 1 ];
    GenL := GenL{[2..Length(GenL)]}; 
	r := RangeM[1];
    RangeM := RangeM{[2..Length(RangeM)]};
    possible_images := Filtered( r, y -> not y in f[ 3 ] );
    for y in possible_images do
        g := StructuralCopy( f );
        g[ 1 ][ x ] := y; AddSet( g[ 2 ], x ); AddSet( g[ 3 ], y );
        g := CREAM_ExtendAllMonomorphisms( g, L, GenL, M, RangeM, findAll );
        if not g = [] and findAll = false then return g; fi;
        if not g = [] then Append(all_mono, g); fi;
    od;

    return all_mono;
end);

#############################################################################
##  
#F  CreamAllMonomorphismMagmasNC(L, M, findAll) 
##
##  Auxiliary function. 
##  Inputs:
##    L:     magma
##    M:     magma
##    findAll: true if all monomorphisms are to be returned,
##             false if only 1 is needed
##  Returns: a list of all injective monomorophisms from L into M, 
##           or [] if no such monomorphism can be found.
##           No checking is done on the monomorphism

InstallGlobalFunction(CreamAllMonomorphismMagmasNC,
function( L, M, findAll )
    local InvL, InvM, DisL, GenL, RangeM, map, mono, Nomatch;
   
   	if Size(L) > Size(M) then return fail; fi;

	InvL := MAGMAS_InvariantVec(L, []);
	InvM := MAGMAS_InvariantVec(M, []);

	DisL := MAGMAS_Invariants(L);
	GenL := MAGMAS_EfficientGenerators(L, DisL);
	
	# RangeM are the possible mappings from elements in generator of L to elements of M
	RangeM := List(GenL, p->Filtered([1..Length(InvM)],
				   idx->CREAM_CompatibleInvariants(InvL[p], InvM[idx])));

	# Check if some elements in L has no possible mappings to M because there are no compatible invariants in M
	Nomatch := Filtered([1..Length(RangeM)], p->Length(RangeM[p]) = 0);
	if Length(Nomatch) > 0 then return []; fi;
	
    map := 0 * [ 1.. Size( L ) ]; 
    mono := CREAM_ExtendAllMonomorphisms( [ map, [ ], [ ] ], MultiplicationTable( L ),
    								      GenL, MultiplicationTable( M ), RangeM, findAll);

	return mono;
end);


#############################################################################
##  
#O  CreamMonomorphismMagmas(L, M)
##  Inputs:
##    L:   a magma
##    M:   a magma
##  Returns: If there is an injective homomorphism from magmas L to M, then it
##    returns such a homomorphism. Otherwise, it returns fail.

InstallMethod(CreamMonomorphismMagmas, "for two magmas",
    [IsMagma, IsMagma],
function(L, M)
	local mono;
	
    mono := CreamAllMonomorphismMagmasNC(L, M, false);
    if mono = fail or Length(mono) = 0 then return fail; fi;
    return mono[1];
end);


#############################################################################
##  
#O  CreamMonomorphismAlgebras(A, B)
##  Inputs:
##    A, B: 2 list of algebras, with binary, unary operations and constants
##		in the following format: 
##				[ b1, b2, b3, ..., u1, u2, ..., c1, c2, ...]
##          where b1, b2, b3 ... are binary operations represented by their
##			 multiplication tables, u1, u2, ... are unary functions represented
##			 by lists of n elements, and c1, c2, ... are constants represented
##			 by lists of single element.
##  Returns: If there is an injective homomorphism from each magma in A to the
##    corresponding magma in B, then it returns such a homomorphism. Otherwise,
##    it returns fail.

InstallMethod(CreamMonomorphismAlgebras, "list of type 2 algebras",
    [IsList, IsList],
function(A, B)
	local L, M, U, C, binop, unop, zeroop, magma, x, y, a, b, c, n, p3, mono_map_list, mono_map, has_mono;
	
	if Length(A) <> Length(B) then return fail; fi;

	M := [];    # multiplication table of binary ops
	L := [];    # magmas of the binary ops
	U := [];    # multiplication table of unary ops
	C := [];    # constants, ops of 0-ary
	for x in [A, B] do  # x is an algebra
		binop := [];
		magma := [];
		unop := [];
		zeroop := [];
		for y in x do   # y is a binop or an unary op or a constant
			if IsList(y[1]) then   # found a bin op
				Add(binop, y);
				Add(magma, MagmaByMultiplicationTable(y));
			elif Length(y) > 1 then
				Add(unop, y);
			else
				Add(zeroop, y);
			fi;
		od;
		Add(M, binop);  # multiplication table of the binary ops M[1] for A, M[2] for B
		Add(U, unop);
		Add(L, magma);  # magmas of the binary ops
		Add(C, zeroop); # constants
	od;
	
	a := Length(U[1]);
	b := Length(L[1]);
	c := Length(C[1]);
	n := Length(M[1][1]);

	# check both algebras are of the same shape
	if a <> Length(U[2]) then return fail; fi;
	if b <> Length(L[2]) then return fail; fi;
	if c <> Length(C[2]) then return fail; fi;
	if n > Length(M[2][1]) then return fail; fi;  # must be able to be injecti ve
	
	x := L[1][1];
	y := L[2][1];

	mono_map_list := CreamAllMonomorphismMagmasNC(x, y, true);
	if mono_map_list = [] then return fail; fi;

	for mono_map in mono_map_list do
		has_mono := true;
		for p3 in [1..c] do
			if mono_map[C[1][p3][1]] <> C[2][p3][1] then
				has_mono := false;
				break;
			fi;
		od;
		if has_mono then
			for p3 in [1..a] do
				if MAGMAS_CanMapUnary(mono_map, U[1][p3], U[2][p3], n) = 0 then
					has_mono := false;
					break;
				fi;
			od;
		fi;
		if has_mono then
			for p3 in [2..b] do
				if MAGMAS_CanMapBinary(mono_map, M[1][p3], M[2][p3], n) = 0 then
					has_mono := false;
					break;
				fi;
			od;
		fi;
		if has_mono then
			return mono_map;
		fi;
	od;

	return fail;
end);

#############################################################################
##  
#O  CreamAllMonomorphismAlgebras(A, B)
##  Inputs:
##    A, B: 2 list of algebras, with binary, unary operations and constants
##		in the following format: 
##				[ b1, b2, b3, ..., u1, u2, ..., c1, c2, ...]
##          where b1, b2, b3 ... are binary operations represented by their
##			 multiplication tables, u1, u2, ... are unary functions represented
##			 by lists of n elements, and c1, c2, ... are constants represented
##			 by lists of single element.
##  Returns: Returns the injective homomorphisms from each magma in A to the
##    corresponding magma in B. Otherwise, returns an empty list.
##

InstallMethod(CreamAllMonomorphismAlgebras, "list of type 2 algebras",
    [IsList, IsList],
function(A, B)
	local L, M, U, C, binop, unop, zeroop, magma, x, y, a, b, c, n, p3, mono_map_list, mono_map, has_mono, monos;
	
	if Length(A) <> Length(B) then return []; fi;

	M := [];    # multiplication table of binary ops
	L := [];    # magmas of the binary ops
	U := [];    # multiplication table of unary ops
	C := [];    # constants, ops of 0-ary
	for x in [A, B] do  # x is an algebra
		binop := [];
		magma := [];
		unop := [];
		zeroop := [];
		for y in x do   # y is a binop or an unary op or a constant
			if IsList(y[1]) then   # found a bin op
				Add(binop, y);
				Add(magma, MagmaByMultiplicationTable(y));
			elif Length(y) > 1 then
				Add(unop, y);
			else
				Add(zeroop, y);
			fi;
		od;
		Add(M, binop);  # multiplication table of the binary ops M[1] for A, M[2] for B
		Add(U, unop);
		Add(L, magma);  # magmas of the binary ops
		Add(C, zeroop); # constants
	od;
	
	a := Length(U[1]);
	b := Length(L[1]);
	c := Length(C[1]);
	n := Length(M[1][1]);

	# check both algebras are of the same shape
	if a <> Length(U[2]) then return []; fi;
	if b <> Length(L[2]) then return []; fi;
	if c <> Length(C[2]) then return []; fi;
	if n > Length(M[2][1]) then return []; fi;  # must be able to be injecti ve
	
	x := L[1][1];
	y := L[2][1];

	mono_map_list := CreamAllMonomorphismMagmasNC(x, y, true);
	if mono_map_list = [] then return []; fi;

	monos := [];
	for mono_map in mono_map_list do
		has_mono := true;
		for p3 in [1..c] do
			if mono_map[C[1][p3][1]] <> C[2][p3][1] then
				has_mono := false;
				break;
			fi;
		od;
		if has_mono then
			for p3 in [1..a] do
				if MAGMAS_CanMapUnary(mono_map, U[1][p3], U[2][p3], n) = 0 then
					has_mono := false;
					break;
				fi;
			od;
		fi;
		if has_mono then
			for p3 in [2..b] do
				if MAGMAS_CanMapBinary(mono_map, M[1][p3], M[2][p3], n) = 0 then
					has_mono := false;
					break;
				fi;
			od;
		fi;
		if has_mono then
			Add (monos, mono_map);
		fi;
	od;

	return monos;

end);





