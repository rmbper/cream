#############################################################################
##
#W  magmaut.gd  Automorphims and Isomorphisms for magmas
##  
##
#Y  Copyright (C)  2020,  João Araújo (Universidade Nova de Lisboa, Portugal),
#Y                        Choiwah Chow (Universidade Aberta, Portugal)
##  
##

#############################################################################
##  INVARIANTS
##  -------------------------------------------------------------------------

############################################################################
##  
#F  MAGMAS_IsLatinSquare(Q) 
##  
##  Auxiliary function  
##  This function checks whether a magma is a latin square
##  Inputs:
##     Q: Multiplication table of a magma as a list of lists.
##  returns: true if the multiplication table is a latin square.

InstallGlobalFunction(MAGMAS_IsLatinSquare,
function( Q )
	local row, col, x, y, n;
	n := Size(Q);
	for x in [1..n] do
		row := 0*[1..n];
		col := 0*[1..n];
    	for y in [1..n] do
    		if row[Q[x][y]] > 0 then return false; else row[Q[x][y]] := 1; fi;
    		if col[Q[y][x]] > 0 then return false; else col[Q[y][x]] := 1; fi;
    	od;
    od;
    return true;
end);


#############################################################################
##  
#F  MAGMAS_InvariantVec(Q, Opt) 
##    
##  Returns the Invariant vector of a Magma Q, represented by a list of lists
##  vec[1..n] such that vec[i] is the invariant vector (list of 18 invariants)
##  of the domain element i.
## 
##  It is based on very similar ideas in the loops package, but adapted
##  to magmas. Many of the functions in the loops package can be adapted to
##  work on magmas, and consequently, many functions in this package are simple
##  modifications of the corresponding functions in the loops package.
##
##  For groups, an optimal subset is {1, 12, 13, 15}
##  For quasigroups, an optimal subset is {1, 12, 14, 17}
############################################################################


InstallGlobalFunction(MAGMAS_InvariantVec,
function(Q, Opt) 
    # This function calculates 18 invariants for a magma, which is given 
    # as an object of the category Magma.
    # It mimics the Discriminator function of the Loops package.
    # Vec[i] will contain the invariant vector for ith element of M.
    
    local i_no, n, M, Vec, el, El2, ElPowers, new_power, pow,
    	  idemp_no, top, Idempotents, vec_size, xdx, ydx, et, tf, t, struct;
    
    M := MultiplicationTable(Q);
    n := Size(M);
    vec_size := 17+1;

    Vec := List( [1..n], idx -> 0*[1..vec_size] );
    
    # Square of domain elements
	El2 := List( [1..n], idx -> M[idx][idx] );
	
	struct := 0;
	if Length(Opt) = 0 then 
		if IsGroup(Q) then
			Opt := [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 13, 0, 15, 0, 0];
			struct := 1;
		elif MAGMAS_IsLatinSquare(M) then
			Opt := [ 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 12, 0, 14, 0, 0, 17 ];
			struct := 3;
		else
			Opt := [1..vec_size];
			struct := 4;
		fi;
	fi;
	for el in [1..n] do Vec[el][vec_size] := struct; od;
 
	i_no := 1;

	# 1. Invariant: min exponent 
	# what is the smallest exponent that recurs, that is, s^n = s^k such that n > k > 1.
    # n is clearly <= domain size + 1
    # Note that s^n = (..(s*s)*s)*s)... for n of the s.
	if Opt[i_no] > 0 then
		for el in [1..n] do
			new_power := el;
			ElPowers := [new_power];
			for pow in [2..n+2] do
				new_power := M[new_power][el];
				if new_power in ElPowers then
					Vec[el][i_no] := pow;
					break;
				fi;
				Add(ElPowers, new_power);
			od;
		od;
		#top := List( [1..n+1], idx-> []);
		#for el in [1..n] do Add(top[Vec[el][1]], el); od;
	fi;
	# if Opt = 1 then for el in [1..n] do Add(top[Vec[el][1]], el); od; fi;

	i_no := i_no + 1;

	# 2. Invariant: Left Indentity
	# Number of elements I am a left identity
	if Opt[i_no] > 0 then
		for el in [1..n] do
			Vec[el][i_no] := Length( Filtered( [1..n], jdx -> M[el][jdx] = jdx ) );
		od;
	fi;
	
	i_no := i_no + 1;

	# 3. Invariant: Right Indentity
	# Number of elements I am a right identity
	if Opt[i_no] > 0 then
		for el in [1..n] do
			Vec[el][i_no] := Length( Filtered( [1..n], jdx -> M[jdx][el] = jdx ) );
		od;
	fi;
	
	#i_no := i_no + 1;

	# discarded 4. Invariant: 2-sided Indentity for all elements
	# 1 if I am the identity for all elements, 0 otherwise
	#for el in [1..n] do
	#	if Vec[el][i_no-1] = n and Vec[el][i_no-2] = n then
	#		Vec[el][i_no] := 1;
	#		break;    # there can only be one identity element
	#	fi;
	#od;

	i_no := i_no + 1;

	# 4. Invariant: number of inverses
	# For each element x, number of elements y such that x = (xy)x
	if Opt[i_no] > 0 then
		for el in [1..n] do
			Vec[el][i_no] := Length( Filtered( [1..n], jdx -> M[M[el][jdx]][el] = el ) );
		od;
	fi;

	i_no := i_no + 1;

	# 5. Invariant: distinct row elements
	# Number of distinct elements on a row
	if Opt[i_no] > 0 then
		for el in [1..n] do
			Vec[el][i_no] := Length( Unique( M[el] ) );
		od;
	fi;
	
	i_no := i_no + 1;

	# 6.Invariant: distinct column elements
	# Number of distinct elements on a column
	if Opt[i_no] > 0 then
		for el in [1..n] do
			Vec[el][i_no] := Length( Unique( List( [1..n], idx -> M[idx][el] ) ) );
		od;
	fi;

	i_no := i_no + 1;
	idemp_no := i_no;
	# 7. Invariant: idempotent
	# Am I idempotent? 1 or 0 
	if Opt[i_no] > 0 or Opt[i_no+1] > 0 or Opt[i_no+2] > 0 then
		for el in [1..n] do
			if El2[el] = el then Vec[el][i_no] := 1; fi;
		od;
	fi;

	i_no := i_no + 1;	
	
	# 8. Invariant: number of idempotents in a column
	# how many in a column is an idempotent i.e. for a fixed a, how many m such that
	# ma is idempotent
	if Opt[i_no] > 0 then
		for el in [1..n] do
			Vec[el][i_no] := Length( Filtered( [1..n], jdx -> Vec[M[jdx][el]][idemp_no] = 1 ) );
		od;
	fi;

	i_no := i_no + 1;
	
	# 9. Invariant: number of idempotents in a row
	# how many in a row is an idempotent i.e. for a fixed a, how many m such that
	# am is idempotent
	if Opt[i_no] > 0 then
		for el in [1..n] do
			Vec[el][i_no] := Length( Filtered( [1..n], jdx -> Vec[M[el][jdx]][idemp_no] = 1 ) );
		od;
	fi;

	i_no := i_no + 1;

	# 10. Invariant: self associative
	# Am I self associative: x(xx) = (xx)x? 1 or 0
	if Opt[i_no] > 0 then
		for el in [1..n] do
			if M[el][El2[el]] = M[El2[el]][el] then Vec[el][i_no] := 1; fi;
		od;
	fi;

	i_no := i_no + 1;

	# 11. Invariant: centralizer 
	# How many elements do I commute with
	# if Opt[i_no] > 0 then
	#	for el in [1..n] do
	#		Vec[el][i_no] := List(top, x -> Length( Filtered( x, jdx -> M[el][jdx] = M[jdx][el] ) ) );
	#	od;
	#fi;
	if Opt[i_no] > 0 then
		for el in [1..n] do
			Vec[el][i_no] := Length( Filtered( [1..n], jdx -> M[el][jdx] = M[jdx][el] ) );
		od;
	fi;

	i_no := i_no + 1;
	
	# 12. Invariant: double centralizer 
	# How many elements for which the square I commute with?
	if Opt[i_no] > 0 then
		for el in [1..n] do
			Vec[el][i_no] := Length( Filtered( [1..n], jdx -> M[el][El2[jdx]] = M[El2[jdx]][el] ) );
		od;
	fi;

	i_no := i_no + 1;
	
	# 13. Invariant: square root 
	# number of element of which I am a square root
	if Opt[i_no] > 0 then
		for el in [1..n] do
			Vec[el][i_no] := Length( Filtered( [1..n], jdx -> El2[jdx] = el ) );
		od;
	fi;

	i_no := i_no + 1;

	# 14. Invariant: associatizer
	# Number of elements y satisfying x(xy) = (xx)y
	if Opt[i_no] > 0 then
		for el in [1..n] do
			Vec[el][i_no] := Length( Filtered( [1..n], jdx -> M[el][M[el][jdx]] = M[El2[el]][jdx] ) );
		od;
	fi;
	
	i_no := i_no + 1;
	
	# 15. Invariant: product of commutors
	# Number of products xy = yx = s	
	if Opt[i_no] > 0 then
		for xdx in [1..n] do
			for ydx in [1..n] do
				if M[xdx][ydx] = M[ydx][xdx] then
					Vec[M[xdx][ydx]][i_no] := Vec[M[xdx][ydx]][i_no] + 1;
				fi;
			od;
		od;
	fi;

	i_no := i_no + 1;

	# 16. Invariant: product of idempotents
	# Number of t such that two idempotents e,f in M and s=et=tf
	if Opt[i_no] > 0 then
		Idempotents := Filtered( [1..n], el->El2[el] = el );
	
		et := 0*[1..n];
		tf := 0*[1..n];
		for t in [1..n] do
			et[t] := List(Idempotents, e->M[e][t]);
			tf[t] := List(Idempotents, f->M[t][f]);
		od;
		for el in [1..n] do
			for xdx in et[el] do
				for ydx in tf[el] do
					if xdx = ydx then
						Vec[xdx][i_no] := Vec[xdx][i_no] + 1;
					fi;
				od;
			od;
		od;
	fi;

	i_no := i_no + 1;	

	# 17. Invariant: ordering
	# the number of t for which there exist two elements x,y in M such that s=xy and t=yx
	if Opt[i_no] > 0 then
		for el in [1..n] do Vec[el][i_no] := []; od;
		for xdx in [1..n] do
			for ydx in [1..n] do
				Add(Vec[M[xdx][ydx]][i_no], M[ydx][xdx]);
			od;
		od;
		for el in [1..n] do
			Vec[el][i_no] := Length( Unique( Vec[el][i_no] ) );
		od;
	fi;
	return Vec;
end);

#############################################################################
##  
#F  MAGMAS_Invariants_G(Q, Opt) 
##    
##  Returns the Invariant vector of a Magma Q, which is a list [ A, B ], 
##  where A is a list of the form
##  [ [I1,n1], [I2,n2],.. ], where the invariant Ii occurs ni times in Q,
##  and where B[i] is a subset of [1..Size(Q)] corresponding to elements of
##  Q with invariant Ii.
## 
##  It is based on very similar ideas in the loops package, but adapted
##  to magmas. Many of the functions in the loops package can be adapted to
##  work on magmas, and consequently, many functions in this package are simple
##  modifications of the corresponding functions in the loops package.
##
##  For groups, an optimal subset is {1, 12, 13, 15}
##  For quasigroups, an optimal subset is {1, 12, 14, 17}
############################################################################


InstallGlobalFunction(MAGMAS_Invariants_G,
function(Q, Opt)	
    local A, B, P, Vec, n;
 
 	n := Size(Q);
	Vec := MAGMAS_InvariantVec(Q, Opt);
	# invariants with the number of occurence
	A := Collected(Vec);  # list of invariant vector with freqency (number of domain elements having that key).
	P := Sortex( List( A, inv -> inv[2] ) );
	A := Permuted(A, P);
	
	# blocks of elements invariant under isomorphisms
	# i.e. B is a list of list of elements that can be isomorphic, 
	# and elements in different lists cannot be isomorphic
    B := List( [1..Length(A)], jdx -> Filtered( [1..n], idx -> Vec[idx] = A[jdx][1] ) );
	
	return [A, B];
end);


############################################################################
##  
#O  MAGMAS_Invariants(Q) 
##  
##  Auxiliary function
##  Inputs:
##     Q:    a magma
##  returns: The invariant vector of Q.

InstallMethod(MAGMAS_Invariants, "for magmas",
    [IsMagma],
function( Q )
	local MT_Q, n;
	MT_Q := MultiplicationTable( Q );
	n := Length(MT_Q);

	return MAGMAS_Invariants_E(Q, MT_Q, n);
end);


############################################################################
##  
#O  MAGMAS_GroupInvariants(Q) 
##  
##  Auxiliary function
##  Inputs:
##     Q:    a Group
##  returns: The invariant vector of Q.

InstallMethod(MAGMAS_GroupInvariants, "for groups",
    [IsGroup],
function( Q )
	local A, P, B, Vec, n, ptr, sg, x, y;
	Vec := MAGMAS_InvariantVec(Q, []);
	
	n := Order(Q);
	sg := [Socle(Q), FrattiniSubgroup(Q), DerivedSubgroup(Q), FittingSubgroup(Q)];
	ptr := Length(Vec[1])-1;
	for x in [1..n] do
		for y in [1..Length(sg)] do
			Vec[x][ptr+y] := Length( Filtered(Elements(sg[y]), el-> Elements(Q)[x] * el = el * Elements(Q)[x]) );
		od;
	od;
	
	# invariants with the number of occurrence
	A := Collected(Vec);  # list of invariant vector with frequency (number of domain elements having that key).
	P := Sortex( List( A, inv -> inv[2] ) );
	A := Permuted(A, P);
	
	# blocks of elements invariant under isomorphisms
	# i.e. B is a list of list of elements that can be isomorphic, 
	# and elements in different lists cannot be isomorphic
    B := List( [1..Length(A)], jdx -> Filtered( [1..n], idx -> Vec[idx] = A[jdx][1] ) );
	
	return [A, B];
end);


############################################################################
##  
#O  MAGMAS_GroupExtendedInvariants(Q) 
##  
##  Auxiliary function
##  Inputs:
##     Q:    a Group
##  returns: The invariant vector of Q.
##	For groups, an optimal subset is {1, 12, 13, 15}

InstallMethod(MAGMAS_GroupExtendedInvariants, "for groups",
    [IsGroup],
function( Q )
	local A, P, B, Vec, MT_Q, n, ptr, sg, el, x, y, pow, orders, El2;
	Vec := MAGMAS_InvariantVec(Q, []);

	MT_Q := MultiplicationTable(Q);
	n := Order(Q);
	# 1 is order, 11 is centralizer
	ptr := Length(Vec[1]);
	Vec[1][1] := 1;
	for el in [2..n] do
		pow := el;
		for y in [2..n] do
			pow := MT_Q[el][pow];
			if pow = el then break; fi;
		od;
		Vec[el][1] := y;
	od;
	orders := Unique(List([1..n], idx->Vec[idx][1]));
	orders := List( [1..Length(orders)], jdx -> Filtered( [1..n], idx -> Vec[idx][1] = orders[jdx] ) );

	# 11. Invariant: centralizer 
	# How many elements do I commute with elements of each order
	for el in [1..n] do
		Vec[el][11] := List( orders, idx -> Length( Filtered( idx, jdx -> MT_Q[el][jdx] = MT_Q[jdx][el] ) ) );
	od;

	El2 := List( [1..n], idx -> MT_Q[idx][idx] );
	
	# 12. Invariant: double centralizer 
	# How many elements for which the square I commute with?
	for el in [1..n] do
		Vec[el][12] := List( orders, idx -> Length( Filtered( idx, jdx -> MT_Q[el][El2[jdx]] = MT_Q[El2[jdx]][el] ) ) );
	od;

	# 13. Invariant: square root 
	# number of element of which I am a square root
	for el in [1..n] do
		Vec[el][13] := List( orders, idx -> Length( Filtered( idx, jdx -> El2[jdx] = el ) ) );
	od;

	sg := [Socle(Q), FrattiniSubgroup(Q), DerivedSubgroup(Q), FittingSubgroup(Q)];
	ptr := 2;
	for x in [1..n] do
		for y in [1..Length(sg)] do
			Vec[x][ptr+y] := Length( Filtered(Elements(sg[y]), el-> Elements(Q)[x] * el = el * Elements(Q)[x]) );
		od;
	od;
	
	# invariants with the number of occurrence
	A := Collected(Vec);  # list of invariant vector with frequency (number of domain elements having that key).
	P := Sortex( List( A, inv -> inv[2] ) );
	A := Permuted(A, P);
	
	# blocks of elements invariant under isomorphisms
	# i.e. B is a list of list of elements that can be isomorphic, 
	# and elements in different lists cannot be isomorphic
    B := List( [1..Length(A)], jdx -> Filtered( [1..n], idx -> Vec[idx] = A[jdx][1] ) );
	
	return [A, B];
end);


############################################################################
##  
#F  MAGMAS_Invariants_E(Q, MT_Q, n) 
##  
##  Auxiliary function
##  Inputs:
##     Q:    a magma
##     MT_Q: Multiplication Table of the magam
##     n:    Size of Q
##  returns: The invariant vector of Q.
##  This GAP function calls C function to do some of the calculations

InstallGlobalFunction(MAGMAS_Invariants_E,
function( Q, MT_Q, n )

	local A, P, B, Vec, struct_type;
	if IsGroup(Q) then
		struct_type := 1;
	else
		struct_type := 0;
	fi;

	Vec := MAGMAS_Invariants_C(MT_Q, struct_type);
	
	# invariants with the number of occurrence
	A := Collected(Vec);  # list of invariant vector with frequency (number of domain elements having that key).
	P := Sortex( List( A, inv -> inv[2] ) );
	A := Permuted(A, P);
	
	# blocks of elements invariant under isomorphisms
	# i.e. B is a list of list of elements that can be isomorphic, 
	# and elements in different lists cannot be isomorphic
    B := List( [1..Length(A)], jdx -> Filtered( [1..n], idx -> Vec[idx] = A[jdx][1] ) );
	
	return [A, B];
end);


############################################################################
##  
#F  MAGMAS_Invariants_E2(Q, MT_Q, n) 
##  
##  Auxiliary function
##  Inputs:
##     Q:    a magma
##     MT_Q: Multiplication Table of the magam
##     n:    Size of Q
##  returns: The invariant vector of Q.
##  This GAP function calls C function to do some of the calculations

InstallGlobalFunction(MAGMAS_Invariants_E2,
function( Q, MT_Q, n )

	local A, P, B, Vec, struct_type, VL, el, sM, Ms;
	if IsGroup(Q) then
		struct_type := 1;
	else
		struct_type := 0;
	fi;

	Vec := MAGMAS_Invariants_C(MT_Q, struct_type);
	VL := Length(Vec[1]);
	sM := 0*[1..n];
	Ms := 0*[1..n];
	for el in [1..n] do
		sM[el] := Set(MT_Q[el]);
		Ms[el] := Set(List([1..n], p->MT_Q[p][el]));
	od;
	for el in [1..n] do
		Vec[el][VL+1] := Length(Filtered(sM, p->IsSubsetSet(sM[el], p)));
		Vec[el][VL+2] := Length(Filtered(Ms, p->IsSubsetSet(Ms[el], p)));
	od;
	
	
	# invariants with the number of occurrence
	A := Collected(Vec);  # list of invariant vector with frequency (number of domain elements having that key).
	P := Sortex( List( A, inv -> inv[2] ) );
	A := Permuted(A, P);
	
	# blocks of elements invariant under isomorphisms
	# i.e. B is a list of list of elements that can be isomorphic, 
	# and elements in different lists cannot be isomorphic
    B := List( [1..Length(A)], jdx -> Filtered( [1..n], idx -> Vec[idx] = A[jdx][1] ) );
	
	return [A, B];
end);


#############################################################################
##  
#O  AreEqualMAGMA_Invariants(D, E) 
##    
##  Returns true if the invarinats of the two invariant vectors are the same,
##  including number of occurrences of each invariant.
##  This function mimics the AreEqualDiscriminators function in the loops package.
 
InstallMethod(AreEqualMAGMA_Invariants, "for two lists (invariant vectors)",
    [ IsList, IsList ],
function(D, E)
      return D[1] = E[1];
end);


############################################################################
##  CORE ISOMORPHISMS
##  -------------------------------------------------------------------------

############################################################################
##  
#F  MAGMAS_SublistPosition(S, x) 
##  
##  Auxiliary function  
##  This function is adapted from LOOPS_SublistPosition in the loops package.
##  Inputs:
##     S: list of lists
##     x: an element to find in S
##  returns: the ith list in S that contains x; or fail.

InstallGlobalFunction(MAGMAS_SublistPosition,
function( S, x )
    local i;
    for i in [ 1..Length( S ) ] do if x in S[ i ] then return i; fi; od;
    return fail;
end);


#############################################################################
##  
#F  MAGMAS_ExtendHomomorphismByClosingSource(f, L, M) 
##
##  Auxiliary function.
##  Inputs:
##    f: a partial map from L to M
##    L: multiplication table of a magma
##    M: multiplication table of a magma
##  This function attempts to extend f into a homomorphism of magma by 
##  extending the source of f into (the smallest possible) submagma of L.
##  This function is adapted from LOOPS_ExtendHomomorphismByClosingSource in
##  the loops package.

InstallGlobalFunction(MAGMAS_ExtendHomomorphismByClosingSource,
function(f, L, M)
    local oldS, newS, pairs, x, y, newNow, p, z, fz;    
    oldS := [ ];
    newS := f[ 2 ];

    repeat  
        pairs := [];
        for x in oldS do for y in newS do 
            Add( pairs, [ x, y ] ); 
            Add( pairs, [ y, x ] );
        od; od;
        for x in newS do for y in newS do
            Add( pairs, [ x, y ] );
        od; od;
        newNow := [];
        for p in pairs do
            x := p[ 1 ];
            y := p[ 2 ];
            z := L[ x ][ y ];
            fz := M[ f[ 1 ][ x ] ][ f[ 1 ][ y ] ];
            if f[ 1 ][ z ] = 0 then
                f[ 1 ][ z ] := fz; AddSet( f[ 2 ], z ); AddSet( f[ 3 ], fz );
                Add( newNow, z );
            else 
                if not f[ 1 ][ z ] = fz then return fail; fi;
            fi;
        od;
        oldS := Union( oldS, newS );
        newS := ShallowCopy( newNow );
    until IsEmpty( newS );
    return f;           
end);


#############################################################################
##  
#F  MAGMAS_ExtendIsomorphism( f, L, GenL, DisL, M, DisM, failure_count ) 
##  
##  Auxiliary function.
##  Inputs:
##		f:  a partial map from L to M
##	    L:  multiplication table of a magma (domain)
##      GenL : partial efficient generators of L - only generators not already used are here
##      DisL : list of blocks of elements with the same invariant vector of L
##      M    : multiplication table of a magma (range)
##      DisM : list of blocks of elements with the same invariant vector of M
##  Returns: an isomorphism between L and M, fail if it cannot find one.
##  This function attempts to extend f into an isomorphism between L and M.
##  This function is adapted from the LOOPS_ExtendIsomorphism in the loops package

InstallGlobalFunction(MAGMAS_ExtendIsomorphism,
function(f, L, GenL, DisL, M, DisM, failure_count)
    local x, possible_images, y, g;
    f := MAGMAS_ExtendHomomorphismByClosingSource( f, L, M );
    if f = fail or Length( f[ 2 ] ) > Length( f[ 3 ] ) then failure_count[1] := failure_count[1] + 1; return fail; fi;
    if Length( f[ 2 ] ) = Length( L ) then return f; fi;
    
    x := GenL[ 1 ];
    GenL := GenL{[2..Length(GenL)]}; 
    possible_images := Filtered( DisM[ MAGMAS_SublistPosition( DisL, x ) ], y -> not y in f[ 3 ] );    
    for y in possible_images do
        g := StructuralCopy( f );
        g[ 1 ][ x ] := y; AddSet( g[ 2 ], x ); AddSet( g[ 3 ], y );
        g := MAGMAS_ExtendIsomorphism( g, L, GenL, DisL, M, DisM, failure_count );
        if not g = fail then return g; fi;
    od;
    return fail;
end);


############################################################################
##  ISOMORPHISMS
##  -------------------------------------------------------------------------

#############################################################################
##  
#O  IsomorphismMagmasNC(L, GenL, DisL, M, DisM) 
##
##  Auxiliary function. 
##  Inputs:
##    L:     magma
##    GenL:  efficient generators of L
##    DisL:  invariant vector of L
##    M:     magma
##    DisM:  invariant vector of M
##  Returns: an isomorophism from L onto M, or fail if they are not isomorphic.
##           No checking is done on the isomorphism

InstallGlobalFunction(IsomorphismMagmasNC,
function( L, GenL, DisL, M, DisM )
    local map, iso, failure_count;

	failure_count := [0];
    map := 0 * [ 1.. Size( L ) ]; 
    iso := MAGMAS_ExtendIsomorphism( [ map, [ ], [ ] ], MultiplicationTable( L ), GenL, DisL[2],
    								 MultiplicationTable( M ), DisM[2], failure_count );

    if not iso = fail then return SortingPerm( iso[ 1 ] ); fi;
    return fail;
end);


#############################################################################
##  
#O  IsomorphismMagmas(L, M)
##  Inputs:
##    L:   a magma
##    M:   a magma
##  Returns: If the magmas L and M are isomorophic, then it returns an isomorphism
##  from L onto M. Otherwise, it returns fail.

InstallMethod(IsomorphismMagmas, "for two magmas",
    [IsMagma, IsMagma],
function(L, M)
    local GenL, DisL, DisM; 
   
    DisL := MAGMAS_Invariants_G(L, []);
    GenL := MAGMAS_EfficientGenerators(L, DisL);
    DisM := MAGMAS_Invariants_G(M, []);
    
    if not AreEqualMAGMA_Invariants(DisL, DisM) then return fail; fi;

    return IsomorphismMagmasNC(L, GenL, DisL, M, DisM);
end);


#############################################################################
##  
#O  MagmasUpToIsomorphism_EqualInvariants(L)
##  Inputs:
##    L: list of magmas, all of which have the same part 1 in 
##		 their invariant vectors
##  Returns: List of all non-isomorphic magmas in L.

InstallMethod(MagmasUpToIsomorphism_EqualInvariants, "list of magmas",
    [IsList],
function(L)
	local inv, x, y, p1, p2, GenL, is_iso, non_iso;
	
	if Length(L) <= 1 then return L; fi;
	
	non_iso := [];
	inv := 0*[1..Length(L)];
	for p1 in [1..Length(L)-1] do
		x := L[p1];
		if inv[p1] = 0 then
			inv[p1] := MAGMAS_Invariants(x);
		fi;
		GenL := MAGMAS_EfficientGenerators(x, inv[p1]);
		is_iso := 0;
		for p2 in [p1+1..Length(L)] do
			y := L[p2];
			if inv[p2] = 0 then
				inv[p2] := MAGMAS_Invariants(y);
			fi;
			if IsomorphismMagmasNC(x, GenL, inv[p1], y, inv[p2]) <> fail then is_iso := 1; break; fi;
		od;
		if is_iso = 0 then Add(non_iso, x); fi;
	od;
	Add(non_iso, L[Length(L)]);
	return non_iso;
end);


#############################################################################
##  
#F  MAGMAS_ExtendAllIsomorphisms( f, L, GenL, DisL, M, DisM ) 
##  
##  Auxiliary function.
##  Inputs:
##		f:  a partial map from L to M
##	    L:  multiplication table of a magma (domain)
##      GenL : partial efficient generators of L - only generators not already used are here
##      DisL : list of blocks of elements with the same invariant vector of L
##      M    : multiplication table of a magma (range)
##      DisM : list of blocks of elements with the same invariant vector of M
##  Returns: an isomorphism between L and M, fail if it cannot find one.
##  This function attempts to extend f into an isomorphism between L and M.
##  This function is adapted from the LOOPS_ExtendIsomorphism in the loops package

InstallGlobalFunction(MAGMAS_ExtendAllIsomorphisms,
function(f, L, GenL, DisL, M, DisM)
    local x, possible_images, y, g, all_iso;
    all_iso := [];
    if Length(GenL) = 0 then
    	f := MAGMAS_ExtendHomomorphismByClosingSource( f, L, M );
    	# if f = fail or Length( f[ 2 ] ) > Length( f[ 3 ] ) then return []; fi;
    	if not f = fail and Length( f[ 2 ] ) = Length( L ) then return [f[1]]; fi;
    	return [];
    fi;

    x := GenL[ 1 ];
    GenL := GenL{[2..Length(GenL)]}; 
    possible_images := Filtered( DisM[ MAGMAS_SublistPosition( DisL, x ) ], y -> not y in f[ 3 ] );    
    for y in possible_images do
        g := StructuralCopy( f );
        g[ 1 ][ x ] := y; AddSet( g[ 2 ], x ); AddSet( g[ 3 ], y );
        g := MAGMAS_ExtendAllIsomorphisms( g, L, GenL, DisL, M, DisM );
        if not g = [] then Append(all_iso, g); fi;
    od;
    return all_iso;
end);


############################################################################
##  ISOMORPHISMS
##  -------------------------------------------------------------------------

#############################################################################
##  
#F  AllIsomorphismMagmasNC(L, GenL, DisL, M, DisM) 
##
##  Auxiliary function. 
##  Inputs:
##    L:     magma
##    GenL:  efficient generators of L
##    DisL:  invariant vector of L
##    M:     magma
##    DisM:  invariant vector of M
##  Returns: an isomorophism from L onto M, or fail if they are not isomorphic.
##           No checking is done on the isomorphism

InstallGlobalFunction(AllIsomorphismMagmasNC,
function( L, GenL, DisL, M, DisM )
    local map, iso;

    map := 0 * [ 1.. Size( L ) ]; 
    iso := MAGMAS_ExtendAllIsomorphisms( [ map, [ ], [ ] ], MultiplicationTable( L ), GenL, DisL[2],
    								     MultiplicationTable( M ), DisM[2]);
	return iso;
end);


#############################################################################
##  
#F  MAGMAS_CanMapUnary( map, U1, U2, n ) 
##
##  Auxiliary function.
##  Inputs:
##		map:  an isomorphic map (represented as a list [a, b, c, ...] such that
##				 map[1] = a etc
##	    U1:  a unary function represented as a list 
##      U2:  a unary function represented as a list 
##      n:   size of U1 and U2
##  Returns: 1 if map is an isomorphic function from U1 to U2, 0 otherwise


InstallGlobalFunction(MAGMAS_CanMapUnary,
function( map, U1, U2, n )
	local x;

	for x in [1..n] do
		if not map[U1[x]] = U2[map[x]] then
			return 0;
		fi;
	od;
	return 1;
end);


#############################################################################
##  
#F  MAGMAS_CanMapBinary( map, M1, M2, n ) 
##
##  Auxiliary function.
##  Inputs:
##		map:  an isomorphic map (represented as a list [a, b, c, ...] such that
##				 map[1] = a etc
##	    M1:  a magma 
##      M2:  a magma
##      n:   size of M1 and M2
##  Returns: 1 if map is an isomorphic function from M1 to M2, 0 otherwise


InstallGlobalFunction(MAGMAS_CanMapBinary,
function( map, M1, M2, n )
	local x, y;

	for x in [1..n] do
		for y in [1..n] do
			if not map[M1[x][y]] = M2[map[x]][map[y]] then
				return 0;
			fi;
		od;
	od;
	return 1;
end);

#############################################################################
##  
#O  Algebra2UpToIsomorphism_EqualInvariants(A)
##  Inputs:
##    A:   list of algebras, all of which have equal invariant vector for
##         each binary operation - .
##         E.g. [[b1, b2, b3, b4], [c1, c2, c3, c4]] where bi, ci are magmas with
##         equal invariant vectors, and b4 and d4 are unary operations
##         Magmas are represented by multiplication tables, unary operations 
##		   are represented by lists.
##  Returns: List of all non-isomorphic algebras in A.

InstallMethod(Algebra2UpToIsomorphism_EqualInvariants, "list of type 2 algebras",
    [IsList],
function(A)
	local L, M, U, C, binop, unop, zeroop, magma, inv, x, y, a, b, c, n, p1, p2, p3, GenL, iso_map_list, iso_map, non_iso, has_iso;
		
	if Length(A) <= 1 then return A; fi;
	
	M := [];
	L := [];
	U := [];
	C := [];
	for x in A do  # x is an algebra
		binop := [];
		magma := [];
		unop := [];
		zeroop := [];
		for y in x do   # y is a binop or an unary op or a constant
			if IsList(y[1]) then   # found a bin op
				Add(binop, y);
				Add(magma, MagmaByMultiplicationTable(y));
			elif Length(y) > 1 then
				Add(unop, y);
			else
				Add(zeroop, y);
			fi;
		od;
		Add(M, binop);  # multiplication table of the binary ops
		Add(U, unop);
		Add(L, magma);  # magmas of the binary ops
		Add(C, zeroop); # constants
	od;
	
	non_iso := [];
	b := Length(L[1]);
	if Length(U) > 0 then
		a := Length(U[1]);
	else
		a := 0;
	fi;
	if Length(C) > 0 then
		c := Length(C[1]);
	else
		c := 0;
	fi;

	n := Length(M[1][1]);
	inv := 0 * [1..Length(L)];
	for p1 in [1..Length(L)-1] do
		x := L[p1][1];
		if inv[p1] = 0 then
			inv[p1] := MAGMAS_Invariants(x);
		fi;
		GenL := MAGMAS_EfficientGenerators(x, inv[p1]);
		has_iso := 0;
		for p2 in [p1+1..Length(L)] do	
			y := L[p2][1];
			if inv[p2] = 0 then inv[p2] := MAGMAS_Invariants(y); fi;

			iso_map_list := AllIsomorphismMagmasNC(x, GenL, inv[p1], y, inv[p2]);
			if iso_map_list = [] then continue; fi;
			for iso_map in iso_map_list do				
				has_iso := 1;
				for p3 in [1..c] do
					if iso_map[C[p1][p3][1]] <> C[p2][p3][1] then
						has_iso := 0;
						break;
					fi;
				od;
				if has_iso = 1 then
					for p3 in [1..a] do
						if MAGMAS_CanMapUnary(iso_map, U[p1][p3], U[p2][p3], n) = 0 then
							has_iso := 0;
							break;
						fi;
					od;
				fi;
				if has_iso = 1 then
					for p3 in [2..b] do
						if MAGMAS_CanMapBinary(iso_map, M[p1][p3], M[p2][p3], n) = 0 then
							has_iso := 0;
							break;
						fi;
					od;
				fi;
				if has_iso = 1 then
					break;
				fi;
			od;
			if has_iso = 1 then break; fi;
		od;
		if has_iso = 0 then 
			Add(non_iso, A[p1]);
		fi;
	od;
	Add(non_iso, A[Length(A)]);
	return non_iso;
end);


#############################################################################
##  
#O  IsomorphismAlgebras(A, B)
##  Inputs:
##    A, B: 2 list of algebras, with binary, unary operations and constants
##		in the following format: 
##				[ b1, b2, b3, ..., u1, u2, ..., c1, c2, ...]
##          where b1, b2, b3 ... are binary operations represented by their
##			 multiplication tables, u1, u2, ... are unary functions represented
##			 by lists of n elements, and c1, c2, ... are constants represented
##			 by lists of single element.
##  Returns: The isomorphic function from A to B if A and B are isomorphic,
##			 fail otherwise.

InstallMethod(IsomorphismAlgebras, "list of type 2 algebras",
    [IsList, IsList],
function(A, B)
	local L, M, U, C, binop, unop, zeroop, magma, inv1, inv2, x, y, a, b, c, n, p3, GenL, iso_map_list, iso_map, has_iso;
	
	if Length(A) <> Length(B) then return fail; fi;

	M := [];
	L := [];
	U := [];
	C := [];
	for x in [A, B] do  # x is an algebra
		binop := [];
		magma := [];
		unop := [];
		zeroop := [];
		for y in x do   # y is a binop or an unary op or a constant
			if IsList(y[1]) then   # found a bin op
				Add(binop, y);
				Add(magma, MagmaByMultiplicationTable(y));
			elif Length(y) > 1 then
				Add(unop, y);
			else
				Add(zeroop, y);
			fi;
		od;
		Add(M, binop);  # multiplication table of the binary ops M[1] for A, M[2] for B
		Add(U, unop);
		Add(L, magma);  # magmas of the binary ops
		Add(C, zeroop); # constants
	od;

	if M[1] = [] then
		n := Length(U[1][1]);
		Add (M[1], []);
		Add (M[2], []);
		for x in [1 .. n] do
			Add (M[1][1], U[1][1]);
			Add (M[2][1], U[2][1]);   
		od;
		Add (L[1], MagmaByMultiplicationTable(M[1][1]));
		Add (L[2], MagmaByMultiplicationTable(M[2][1]));
	else
		n := Length(M[1][1]);
	fi;
	
	a := Length(U[1]);
	b := Length(L[1]);
	c := Length(C[1]);
	if M[1] = [] then
		n := Length(U[1][1]);
	else
		n := Length(M[1][1]);
	fi;
	
	if a <> Length(U[2]) then return fail; fi;
	if b <> Length(L[2]) then return fail; fi;
	if c <> Length(C[2]) then return fail; fi;
	if M[1] = [] then
		if n <> Length(U[2][1]) then return fail; fi;
	else
		if n <> Length(M[2][1]) then return fail; fi;
	fi;
	
	if M[1] = [] then
		iso_map_list := Arrangements ( [1 .. n],n);
	else

		x := L[1][1];
		y := L[2][1];

		inv1 := MAGMAS_Invariants(x);
		inv2 := MAGMAS_Invariants(y);
	
		if not AreEqualMAGMA_Invariants(inv1, inv2) then return fail; fi;

		GenL := MAGMAS_EfficientGenerators(x, inv1);

		iso_map_list := AllIsomorphismMagmasNC(x, GenL, inv1, y, inv2);
	fi;
	
	if iso_map_list = [] then return fail; fi;
	for iso_map in iso_map_list do				
		has_iso := 1;
		for p3 in [1..c] do
			if iso_map[C[1][p3][1]] <> C[2][p3][1] then
				has_iso := 0;
				break;
			fi;
		od;
		if has_iso = 1 then
			for p3 in [1..a] do
				if MAGMAS_CanMapUnary(iso_map, U[1][p3], U[2][p3], n) = 0 then
					has_iso := 0;
					break;
				fi;
			od;
		fi;
		if has_iso = 1 then
			if not M[1] = [] then
				for p3 in [2..b] do
					if MAGMAS_CanMapBinary(iso_map, M[1][p3], M[2][p3], n) = 0 then
						has_iso := 0;
						break;
					fi;
				od;
			fi;
		fi;
		if has_iso = 1 then
			return SortingPerm(iso_map);
		fi;
	od;

	return fail;
end);


#############################################################################
##  
#O  MagmasUpToIsomorphism(L)
##  Inputs:
##    L:   list of magmas
##  Returns: List of all non-isomorphic magmas in L.

InstallMethod(MagmasUpToIsomorphism, "list of magmas",
    [IsList],
function(L)
	local non_iso, is_non_iso, x, y, p1, p2;
	
	if Length(L) <= 1 then return L; fi;
	
	non_iso := [];
	for p1 in [1..Length(L)-1] do
		x := L[p1];
		is_non_iso := 1;
		for p2 in [p1+1..Length(L)] do
			y := L[p2];
			if not IsomorphismMagmas(x, y) = fail then is_non_iso := 0; break; fi;
		od;
		if is_non_iso = 1 then
			Add(non_iso, x);
		fi;
	od;
	Add(non_iso, L[Length(L)]);
	return non_iso;
end);

############################################################################
##  AUTOMORPHISMS
##  -------------------------------------------------------------------------

#############################################################################
##  
#F  MAGMAS_EfficientGenerators(Q, D) 
##    
##  Auxiliary function.
##  Inputs:
##      Q:  a magma
##      D:  the invariants of Q
##  Returns:
##     a list of integers that are indices of generators of Q deemed best for
##     an isomorphism filter.
##  This function is adapted from the LOOPS_EfficientGenerators function in
##  the loops package

InstallGlobalFunction(MAGMAS_EfficientGenerators,
function(Q, D) 
    local gens, sub, elements, candidates, max, S, best_gen, best_S;

    gens := [];                             # generating set to be returned
    sub := [];                              # substructure generated so far
    elements := Concatenation( D[2] );      # all elements ordered by block size
    candidates := ShallowCopy( elements );  # candidates for next generator
    while sub <> Q do
        # find an element not in sub that most enlarges sub
        max := 0;
        while not IsEmpty( candidates ) do
            S := Submagma( Q, Elements( Parent( Q ) ){ Union( gens, [candidates[1]] ) } );
            if Size(S) > max then
                max := Size( S );
                best_gen := candidates[1];
                best_S := S;
            fi;
            # discard elements of S since they cannot do better
            candidates := Filtered( candidates, x -> not Elements(Q)[x] in S );
        od;
        Add( gens, best_gen );
        sub := best_S;
        # reset candidates for next round
        candidates := Filtered( elements, x -> not Elements(Q)[x] in sub );
    od;
    return gens;

end);

#############################################################################
##  
#F  MAGMAS_MoreEfficientGenerators(Q, MT_Q, D) 
##    
##  Auxiliary function.
##  Inputs:
##      Q:  a magma
##		MT_Q: multiplication table for Q
##      D:  the invariants of Q
##  Returns:
##     a list of integers that are indices of generators of Q deemed best for
##     an isomorphism filter.
##  This function is adapted from the LOOPS_EfficientGenerators function in
##  the loops package

InstallGlobalFunction(MAGMAS_MoreEfficientGenerators,
function(Q, MT_Q, D) 
    local gens, sub, elements, candidates, max, S, best_gen, best_S, n, sM, Ms, el, Vec, pos;

	n := Length(MT_Q);
	sM := 0*[1..n];
	Ms := 0*[1..n];
	for el in [1..n] do
		sM[el] := Set(MT_Q[el]);
		Ms[el] := Set(List([1..n], p->MT_Q[p][el]));
	od;
	Vec := [0]*n;
	for el in [1..n] do
		Vec[el] := Length(Filtered(sM, p->IsSubsetSet(sM[el], p))) + Length(Filtered(Ms, p->IsSubsetSet(Ms[el], p)));
	od;

	elements := Concatenation( D[2] );
	pos := [0]*n;
	for el in [1..n] do pos[elements[el]] := el; od;

	elements := [1..n];
	Sort(elements, function(u, v) return (Vec[u] > Vec[v]) or (Vec[u] = Vec[v] and pos[u] < pos[v]); end);
	
    gens := [];                             # generating set to be returned
    sub := [];                              # substructure generated so far
    # elements := Concatenation( D[2] );      # all elements ordered by block size
    candidates := ShallowCopy( elements );  # candidates for next generator
    while sub <> Q do
        # find an element not in sub that most enlarges sub
        max := 0;
        while not IsEmpty( candidates ) do
            S := Submagma( Q, Elements( Parent( Q ) ){ Union( gens, [candidates[1]] ) } );
            if Size(S) > max then
                max := Size( S );
                best_gen := candidates[1];
                best_S := S;
            fi;
            # discard elements of S since they cannot do better
            candidates := Filtered( candidates, x -> not Elements(Q)[x] in S );
        od;
        Add( gens, best_gen );
        sub := best_S;
        # reset candidates for next round
        candidates := Filtered( elements, x -> not Elements(Q)[x] in sub );
    od;
    return gens;

end);


#############################################################################
##  
#F  MAGMAS_AutomorphismsFixingSet(S, Q, MT_Q, n, GenQ, DisQ) 
##
##  Auxiliary function.
##  Inputs:
##      Q:  a magma
##      S:  a subset of Q
##      MT_Q: multiplication table of Q
##      n:    size of Q
##      GenQ: efficient generator of Q
##      DisQ: invariant subsets of Q
##  Returns: all automorphisms of Q fixing the set S pointwise.
##  This function is adapted from the LOOPS_AutomorphismsFixingSet function
##  in the loops package
##  It uses a C function to do part of the computations

InstallGlobalFunction(MAGMAS_AutomorphismsFixingSet,
function(S, Q, MT_Q, n, GenQ, DisQ)
    local x, A;
    
    # this is faster than extending a map
    S := Submagma( Q, Elements( Parent( Q ) ){S} );
    if Size( S ) = n then return []; fi;
    S := List( S, x -> Position( Elements( Q ), x ) );
    
    # pruning blocks
    DisQ := List( DisQ, B -> Filtered( B, x -> not x in S ) );
    
    # first unmapped generator
    x := GenQ[ 1 ]; 
    GenQ := GenQ{[2..Length(GenQ)]};
    
    A := MAGMAS_AutomorphismsFixingSet_C(S, MT_Q, x, GenQ, DisQ);
    
    S := Union(S, [x]);
    return Union(A, MAGMAS_AutomorphismsFixingSet(S, Q, MT_Q, n, GenQ, DisQ));  
end);


#############################################################################
##  
#F  MAGMAS_AutomorphismsFixingSet(S, Q, GenQ, DisQ, failure_count) 
##
##  Auxiliary function.
##  Inputs:
##      Q:  a magma
##      S:  a subset of Q
##      GenQ: efficient generator of Q
##      DisQ: invariant subsets of Q
##      failure_count:  
##  Returns: all automorphisms of Q fixing the set S pointwise, and the number
##           of times it fails to extend an homomorphism to the entire magma
##  This function is adapted from the LOOPS_AutomorphismsFixingSet function
##  in the loops package.
##  It is mainly used for investigation and experimentation.

InstallGlobalFunction(MAGMAS_AutomorphismsFixingSet_G,
function(S, Q, GenQ, DisQ, failure_count)
    local n, x, A, possible_images, y, i, map, g;
    
    # this is faster than extending a map
    n := Size( Q );
    S := Submagma( Q, Elements( Parent( Q ) ){S} );
    if Size( S ) = n then return []; fi;
    S := List( S, x -> Position( Elements( Q ), x ) );
    
    # pruning blocks
    DisQ := List( DisQ, B -> Filtered( B, x -> not x in S ) );
    
    # first unmapped generator
    x := GenQ[ 1 ]; 
    GenQ := GenQ{[2..Length(GenQ)]};
    
    A := [];
    
    # For mapping x in automorphism, only need to consider the list of which x is in 
    # because other lists have different invariant vectors.
    possible_images := Filtered( DisQ[ MAGMAS_SublistPosition( DisQ, x ) ], y -> y <> x );   
    for y in possible_images do
        # constructing map
        map := 0*[1..n];
        for i in [1..n] do if i in S then map[ i ] := i; fi; od;
        map[ x ] := y;
        g := [ map, Union( S, [ x ] ), Union( S, [ y ] ) ];
        # extending map
        g := MAGMAS_ExtendIsomorphism( g, MultiplicationTable( Q ), GenQ, DisQ, MultiplicationTable( Q ), DisQ, failure_count );
        if not g = fail then AddSet( A, g[ 1 ] ); fi;  # g[1] is the map, i.e. add the automorphism to results list
    od;
    
    S := Union(S, [x]);
    return Union(A, MAGMAS_AutomorphismsFixingSet_G(S, Q, GenQ, DisQ, failure_count));  
end);


#############################################################################
##  
#O  AutomorphismGroup_G(Q) 
##
##  Inputs:
##    Q:  a magma
##  Returns all automorphims of M, the size of the generating set,
##           the size of the fixing set, and the number of trials that did not extend
##           successful to an automorphism.
##  This is for debugging and for providing more insight into the algorithm

InstallOtherMethod(AutomorphismGroup_G, "for magma",
    [IsMagma],
function(Q)
    local DisQ, GenQ, A, failure_count;
    DisQ := MAGMAS_Invariants_G(Q, []);    
    GenQ := MAGMAS_EfficientGenerators(Q, DisQ);

	failure_count := [0];
    A := MAGMAS_AutomorphismsFixingSet_G( [], Q, GenQ, DisQ[2], failure_count );
    
    if IsEmpty(A) then return Group( () ); fi;
    return [Group( List( A, p->SortingPerm(p) ) ), Length(GenQ), Length(A), failure_count[1]];
end);


#############################################################################
##  
#F  MAGMAS_AutomorphismGroup(Q, C) 
##    
##  Auxiliary function.
##  Inputs:
##      Q:  a magma
##      C:  A list of int representing the choices of invariant vectors to use
##          C[x] = 0 means the x-th invariant vector is not to be used
##			if C i [], then it is up to the function to decide the best options
##  Returns:
##     The automorphism group for Q.
##  This function is adapted from the AutomorphismGroup function in the loops package.
##  It uses some C code.

InstallGlobalFunction(MAGMAS_AutomorphismGroup,
function(Q, C)
    local DisQ, GenQ, A, MT_Q, n;

	MT_Q := MultiplicationTable(Q);
	n := Size(MT_Q);

	if IsEmpty(C) then
    	DisQ := MAGMAS_Invariants_E(Q, MT_Q, n);
    else
    	DisQ := MAGMAS_Invariants_G(Q, C); 
    fi; 
    GenQ := MAGMAS_EfficientGenerators(Q, DisQ);

    A := MAGMAS_AutomorphismsFixingSet( [], Q, MT_Q, n, GenQ, DisQ[2] );
    
    if IsEmpty(A) then return Group( () ); fi;
    return Group( List( A, p->SortingPerm(p) ) );
end);


#############################################################################
##  
#F  MAGMAS_AutomorphismGroup2(Q, C) 
##    
##  Auxiliary function.
##  Inputs:
##      Q:  a magma
##      C:  A list of int representing the choices of invariant vectors to use
##          C[x] = 0 means the x-th invariant vector is not to be used
##			if C i [], then it is up to the function to decide the best options
##  Returns:
##     The automorphism group for Q.
##  This function is adapted from the AutomorphismGroup function in the loops package.
##  It uses some C code.

InstallGlobalFunction(MAGMAS_AutomorphismGroup2,
function(Q, C)
    local DisQ, GenQ, A, MT_Q, n;

	MT_Q := MultiplicationTable(Q);
	n := Size(MT_Q);

	if IsEmpty(C) then
    	DisQ := MAGMAS_Invariants_G(Q, C);   # DisQ := MAGMAS_Invariants_E(Q, MT_Q, n);
    else
    	DisQ := MAGMAS_Invariants_G(Q, C); 
    fi; 
    GenQ := MAGMAS_MoreEfficientGenerators(Q, MT_Q, DisQ);

    A := MAGMAS_AutomorphismsFixingSet( [], Q, MT_Q, n, GenQ, DisQ[2] );
    
    if IsEmpty(A) then return Group( () ); fi;
    return Group( List( A, p->SortingPerm(p) ) );
end);


#############################################################################
##  
#O  AutomorphismGroup(Q) 
##
##  Inputs:
##    Q:  a magma
##  Returns the automorphism group of Q.
##  It mimics the AutomorphismGroup function in the loops package

InstallOtherMethod(AutomorphismGroup, "for magma",
    [IsMagma],
function(Q)
	return MAGMAS_AutomorphismGroup(Q, []);
end);


#############################################################################
##  
#O  Magmaut_AutomorphismGroup(Q) 
##
##  Inputs:
##    Q:  a magma
##  Returns the automorphism group of q.
##  It mimics the AutomorphismGroup function in the loops package

InstallOtherMethod(Magmaut_AutomorphismGroup, "for magma",
    [IsMagma],
function(Q)
	return MAGMAS_AutomorphismGroup(Q, []);
end);


#############################################################################
##  
#O  Magmaut_AutomorphismGroup_G(Q, C) 
##
##  Inputs:
##    Q:  a magma
##    C:  A list of int representing the choices of invariant vectors to use
##          C[x] = 0 means the x-th invariant vector is not to be used
##  Returns the automorphism group of M, length of the generating set, the length
##          of the fixing set, and the number of times the partial hormomorphism do
##          not extend successfully to the complete magma.
##  No C function is used.  This is mainly for investigation and experimentation.

InstallOtherMethod(Magmaut_AutomorphismGroup_G, "for magma",
    [IsMagma, IsList],
function(Q, C)
    local DisQ, GenQ, A, failure_count;
    DisQ := MAGMAS_Invariants_G(Q, C);    
    GenQ := MAGMAS_EfficientGenerators(Q, DisQ);

	failure_count := [0];
    A := MAGMAS_AutomorphismsFixingSet_G( [], Q, GenQ, DisQ[2], failure_count );
    
    if IsEmpty(A) then return [Group( () ), Length(GenQ), 0, failure_count[1]]; fi;
    return [Group( List( A, p->SortingPerm(p) ) ), Length(GenQ), Length(A), failure_count[1]];
end);

#############################################################################
##  
#F  MAGMAS_UnaryCommute(UnOps, t) 
##
##  Auxiliary function.
##  Inputs:
##      UnOps:  a list of of lists.  Each list is a unary function, that is, f(x) = f[x].
##      t:  a unary function represented by a list
##  Returns: true if t commutes (function composition-wise) with every function in UnOps,
##           false otherwise.

InstallGlobalFunction(MAGMAS_UnaryCommute,
function(UnOps, t)
	local u, y;
	for u in UnOps do
		for y in [1..Length(u)] do
			if not t[u[y]] = u[t[y]] then
				return false;
			fi;
		od;
	od;
	return true;
end);


#############################################################################
##  
#O  Algebra_AutomorphismGroup(Qs) 
##
##  Inputs:
##    Qs:  An algebra, represented by a list of of a lists representing binary ops and unary ops of a magma. 
##         Binary ops are represented by 2-D multiplication tables, that is list of lists.
##         Unary function is represented by a list.
##  Returns the automorphism group of the algebra.

InstallOtherMethod(Algebra_AutomorphismGroup, "for magmas",
    [IsList],
function(Qs)
    local binOps, unOps, magmas, vecs, n, ndx, Vec, A, B, P, Q, DisQ, GenQ, C, D;

	if IsEmpty(Qs) then return Group(()); fi;

	Qs := Filtered(Qs, t->Length(t) > 0);
	unOps := Filtered(Qs, t->not(IsList(t[1])));
	binOps := Filtered(Qs, t->IsList(t[1]));
	if IsEmpty(binOps) and IsEmpty(unOps) then return fail; fi;

    D := [];
	if IsEmpty(binOps) then
		return fail;
		#n := Size(unOps[1]);
		#C := AsSet (SymmetricGroup (n));
		#C := List (C, t->ListPerm(t,n));
		#C := Filtered(C, t->MAGMAS_UnaryCommute(unOps, t));
		#if IsEmpty(C) then
		#	return Group( () );
		#else
		#	Add(D, Group( List( C, p->SortingPerm(p) ) ));
		#fi;
	else
		n := Size(binOps[1]);
		
		vecs := List(binOps, t->MAGMAS_Invariants_C(t, 0));
		Vec := List([1..n], t->[]);
		for ndx in [1..n] do
			Vec[ndx] := Concatenation(List(vecs, t->t[ndx]));
		od;
		
		# invariants with the number of occurrence
		A := Collected(Vec);  # list of invariant vector with frequency (number of domain elements having that key).
		P := Sortex( List( A, inv -> inv[2] ) );
		A := Permuted(A, P);
		
		# blocks of elements invariant under isomorphisms
		# i.e. B is a list of list of elements that can be isomorphic, 
		# and elements in different lists cannot be isomorphic
	    B := List( [1..Length(A)], jdx -> Filtered( [1..n], idx -> Vec[idx] = A[jdx][1] ) );
	    
	    DisQ := [A, B];
	    for ndx in [1..Length(binOps)] do
	    	Q := MagmaByMultiplicationTable(binOps[ndx]);
	    	GenQ := MAGMAS_EfficientGenerators(Q, DisQ);
	    	C := MAGMAS_AutomorphismsFixingSet( [], Q, binOps[ndx], n, GenQ, DisQ[2] );
	    	if not IsEmpty(C) and not IsEmpty(unOps) then
	    		C := Filtered(C, t->MAGMAS_UnaryCommute(unOps, t));
			fi;
			if IsEmpty(C) then
				return Group( () );
			else
				Add(D, Group( List( C, p->SortingPerm(p) ) ));
			fi;
	    od;
	fi;
    return Intersection(D);
end);


#############################################################################
##  
#O  Magmaut_GrpAutomorphismGroup(Q) 
##    
##  Auxiliary function.
##  Inputs:
##      Q:  a group
##  Returns:
##     The automorphism group for Q.
##  This function is adapted from the AutomorphismGroup function in the loops package.
##  It uses some C code.

InstallOtherMethod(Magmaut_GrpAutomorphismGroup, "for groups",
    [IsGroup],
function(Q)
    local DisQ, GenQ, A, MT_Q, n, failure_count;

	MT_Q := MultiplicationTable(Q);
	n := Size(MT_Q);

	DisQ := MAGMAS_GroupInvariants(Q);
    GenQ := MAGMAS_EfficientGenerators(Q, DisQ);

	failure_count := [0];
    A := MAGMAS_AutomorphismsFixingSet_G( [], Q, GenQ, DisQ[2], failure_count );
    # A := MAGMAS_AutomorphismsFixingSet( [], Q, MT_Q, n, GenQ, DisQ[2] );
    
    if IsEmpty(A) then return Group( () ); fi;
    return Group( List( A, p->SortingPerm(p) ) );
end);


#############################################################################
##  
#O  Magmaut_GrpExtendedAutomorphismGroup(Q) 
##    
##  Auxiliary function.
##  Inputs:
##      Q:  a group
##  Returns:
##     The automorphism group for Q.
##  This function is adapted from the AutomorphismGroup function in the loops package.
##  It uses some C code.

InstallOtherMethod(Magmaut_GrpExtendedAutomorphismGroup, "for groups",
    [IsGroup],
function(Q)
    local DisQ, GenQ, A, MT_Q, n, failure_count;

	MT_Q := MultiplicationTable(Q);
	n := Size(MT_Q);

	DisQ := MAGMAS_GroupExtendedInvariants(Q);     # MAGMAS_GroupInvariants(Q);
    GenQ := MAGMAS_EfficientGenerators(Q, DisQ);

	# failure_count := [0];
    # A := MAGMAS_AutomorphismsFixingSet_G( [], Q, GenQ, DisQ[2], failure_count );
    A := MAGMAS_AutomorphismsFixingSet( [], Q, MT_Q, n, GenQ, DisQ[2] );
    
    if IsEmpty(A) then return Group( () ); fi;
    return Group( List( A, p->SortingPerm(p) ) );
end);


