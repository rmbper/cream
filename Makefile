GAPARCH=x86_64-pc-linux-gnu-default64-kv3

all: default

default: bin/$(GAPARCH)/cCream.la.so

bin/$(GAPARCH)/cCream.la.so: src/cCream.c
	mkdir -p bin/$(GAPARCH)
	../../gac -o bin/$(GAPARCH)/cCream.la.so -d src/cCream.c 

clean:
	rm -rf bin

distclean: clean
	rm -rf Makefile

doc:
	../../gap -A -q -T < makedocrel.g

.PHONY: all default clean distclean doc
