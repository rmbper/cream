  
  [1X1 [33X[0;0YThe [5XCREAM[105X[101X[1X package[133X[101X
  
  
  [1X1.1 [33X[0;0YIntroduction[133X[101X
  
  [33X[0;0YThis is the manual for the [5XGAP[105X package [5XCREAM[105X, version 2.0.15.[133X
  
  [33X[0;0Y[5XCREAM[105X   2.0.15   provides   efficient  tools  to  compute  the  congruences,
  endomorphisms,  automorphisms  and  isomorphisms  of finite algebras of type
  ([22X2^m[122X,[22X1^n[122X).[133X
  
  [33X[0;0YThe functions in this package work with all such algebras.[133X
  
  
  [1X1.2 [33X[0;0YOverview[133X[101X
  
  [33X[0;0YChapter  [14X2[114X  contains  the  instructions intructions for the installation and
  configuration  of  the  [5XCREAM[105X  package. Chapter [14X3[114X provides some mathematical
  background  and  terminology.  Chapter  [14X4[114X lists the functions of the [5XCREAM[105X's
  package.[133X
  
