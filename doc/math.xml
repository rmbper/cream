<Chapter Label = "background">
  <Heading>Mathematical Background</Heading>
	This Chapter provides a brief description of the basic mathematical background and notation used.

  <!--**********************************************************************-->
  <!--**********************************************************************-->

  <Section Label = "Algebras of type 2">
    <Heading>Algebra of Type (<M>2^m</M>,<M>1^n</M>) </Heading>

	An Universal Algebra is an algebraic structure consisting of a set <M>A</M> 
	together with a collection of operations on <M>A</M> <Cite Key="WUA"/><M>^1</M>.
	An <M>n</M>-ary operation on <M>A</M> is a function that takes <M>n</M>-tuple of elements
	from <M>A</M> and returns a single element of <M>A</M>. For the scope of this package 
	we are only considering operations with arity of <M>1</M> or <M>2</M>, ie. unary and 
	binary operations. An algebra of type  (<M>2^m</M>,<M>1^n</M>) is a universal algebra 
	with <M>m</M> binary and <M>n</M> unary operations. <P/>
	<P/>
	The package represents a finite Universal Algebra as a list of operations. 
	An operation of arity <M>1</M> is represented by a vector, while an 
	operation of arity <M>2</M> is represented by  a square matrix. The 
	underlying set <M>A</M> is implicitly specified by the vector or matrix 
	sizes, which need to agree for a valid representation. If this dimension 
	is <M>d</M>, the algebra is defined on the set <M>A=\{1,\dots,d\}</M>. The 
	vector or matrix describes the corresponding operation by listing all 
	images in the obvious way. The following is an example of a representation 
	for an algebra with a unary and a binary operation.<P/>
<Listing>
<![CDATA[
[ [3, 1, 2], [ [1, 2, 3], [2, 3, 1], [3, 1, 2] ] ]]]></Listing>
-------------------------------------------------------------------------------
<P/>
<M>^1</M> The definition also requires that the operations map to an index 
scheme. As this package only deals with properties of single algebras, this
technical detail is not material. 
  </Section>

  <!--**********************************************************************-->
  <!--**********************************************************************-->

  <Section Label = "Partition">
    <Heading>Partition </Heading>

	A partition of a set <M>A</M> is a collection of non-empty subsets of
	<M>A</M>, such that every element of <M>A</M> is included in exactly one 
	subset <Cite Key="WPS"/>. Each subset in a partition is called a block. 
	For example, <M>\{\{1,3\},\{2,4\}\}</M> is a 2-blocks partition of the 
	set <M>\{1,2,3,4\}</M>.<P/>
	<P/>
	A block of a partition is efficiently represented as a tree. A partition 
	is  represented by a forest, that is, a disjoint union of trees. The 
	forest is physically presented in the computer memory by an array, whose 
	size will be the size of the set <M>A</M>.<P/>
	<P/>
	In a tree each node or leaf has a parent node except for top nodes or
	roots. In the array representation each position of the array represents a
	node and the value of the node points to its parent node. Top nodes should
	have a value indicating that the node is the root of the tree. A negative
	number is used to signal the root where the absolute value of this number is
	the number of elements of a block.<P/>
	<P/>
	A shallow tree is a tree in which all non-root nodes are connected 
	directly to the root. To obtain a unique representation, we adopt the 
	convention that the trees used in presenting a partition are shallow, 
	and that the first node in a block will be the root node. A partition 
	representation that respects the above conventions will be called a 
	normalized (representation of a) partition.<P/>
	<P/>
	Using these conventions, the encoding of a partition into singletons 
	will have the form<P/>
	<P/>
<Listing>
<![CDATA[
[[1], [2], [3], [4], [5], [6]]  -  [-1, -1, -1, -1, -1, -1]
]]></Listing>
	<P/>
	while the encoding of a partition with just one block is<P/>
	<P/>
<Listing>
<![CDATA[
[[1, 2, 3, 4, 5, 6]]  -  [-6, 1, 1, 1, 1, 1]
]]></Listing>
	<P/>
	Other examples are<P/>
	<P/>
<Listing>
<![CDATA[
[[1, 6], [2], [3, 5], [4]] – [-2, -1, -2, -1, 3, 1]
[[1, 3, 5], [2, 6], [4]] – [-3, -2, 1, -1, 1, 2]
[[1, 2, 5, 6], [3, 4]] – [-4, 1, -2, 3, 1, 1]
]]></Listing>
  </Section>

  <!--**********************************************************************-->
  <!--**********************************************************************-->

  <Section Label = "Congruence">
    <Heading>Congruences </Heading>

	A congruence of an algebra is an equivalence relation on the underlying 
	set of an algebra that is compatible with all algebraic operations
	<Cite Key="WCR"/>.<P/>
	<P/>
 	Technically, a congruence relation is an equivalence relation <M>\equiv</M> on an 
	algebra that satisfies <M>\mu(a_1, a_2, ..., a_n)\equiv\mu(a'_1, a'_2, 
	..., a'_n)</M> for every <M>n</M>-ary operation <M>\mu</M>  and all 
	elements <M>a_1, ..., a_n, a'_1, ..., a'_n</M> such that <M>a_i \equiv 
	a'_i</M> for each <M>i=1, ..., n</M>.<P/>
	<P/>
	Every equivalence relation and hence every congruence corresponds to a  
	partition of the underlying set, and hence congruences can be represented 
	by partitions as detailed in the previous section.<P/>
	<P/>
  </Section>

  <!--**********************************************************************-->
  <!--**********************************************************************-->

  <Section Label = "Homo iso and auto morphisms">
    <Heading>Homomorphism, Endomorphism, Isomorphism and Automorphism</Heading>

	If <M>A</M>, <M>B</M> are two algebras of the same type, then a function 
	<M>f:A\to B</M> is a  a <E>homomorphism</E> from <M>A</M> to <M>B</M> if 
	<M>\mu(f(a_1), f(a_2), ..., f(a_n))= f(\mu(a_1, a_2, ..., a_n))</M> for 
	every <M>n</M>-ary  operation <M>\mu</M> and <M>a_1, \dots a_n\in A</M>.
	<M>^2</M><P/>
	<P/>
	An <E>endomorphism</E> is a <E>homomorphism</E> from an algebra to itself,
	a <E>monomorphism</E> is an injective <E>homomorphism</E>, while an 
	<E>isomorphism</E> is a bijective <E>homomorphism</E>. If an 
	<E>isomorphism</E> exists between two algebras, then the two algebras are
	said to be <E>isomorphic</E>. An <E>automorphism</E> is a 
	<E>homomorphisms</E> that is both an <E>isomorphism</E> and an 
	<E>endomorphism</E>.<P/>
	<P/>
-------------------------------------------------------------------------------
<P/>
<M>^2</M> More precisely, <M>\mu</M> here stands for the two operations of <M>A</M> and <M>B</M> that are indexed equally by the (common) index scheme.

  </Section>

  <!--**********************************************************************-->
  <!--**********************************************************************-->

</Chapter>

