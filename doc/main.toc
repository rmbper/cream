\contentsline {chapter}{\numberline {1}\leavevmode {\color {Chapter } The \textsf {CREAM} package }}{4}{chapter.1}
\contentsline {section}{\numberline {1.1}\leavevmode {\color {Chapter } Introduction }}{4}{section.1.1}
\contentsline {section}{\numberline {1.2}\leavevmode {\color {Chapter } Overview }}{4}{section.1.2}
\contentsline {chapter}{\numberline {2}\leavevmode {\color {Chapter }Installing \textsf {CREAM}}}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}\leavevmode {\color {Chapter }Package dependencies}}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}\leavevmode {\color {Chapter }Compiling the kernel module}}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}\leavevmode {\color {Chapter }Compiling mace4}}{5}{section.2.3}
\contentsline {section}{\numberline {2.4}\leavevmode {\color {Chapter }Building the documentation}}{6}{section.2.4}
\contentsline {section}{\numberline {2.5}\leavevmode {\color {Chapter }Testing your installation}}{6}{section.2.5}
\contentsline {chapter}{\numberline {3}\leavevmode {\color {Chapter }Mathematical Background}}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}\leavevmode {\color {Chapter }Algebra of Type ($2^m$,$1^n$) }}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}\leavevmode {\color {Chapter }Partition }}{7}{section.3.2}
\contentsline {section}{\numberline {3.3}\leavevmode {\color {Chapter }Congruences }}{8}{section.3.3}
\contentsline {section}{\numberline {3.4}\leavevmode {\color {Chapter }Homomorphism, Endomorphism, Isomorphism and Automorphism}}{8}{section.3.4}
\contentsline {chapter}{\numberline {4}\leavevmode {\color {Chapter }Functionality}}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}\leavevmode {\color {Chapter }Algebras}}{9}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}\leavevmode {\color {Chapter }CreamSizeAlgebra}}{9}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}\leavevmode {\color {Chapter }CreamIsAlgebra}}{9}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}\leavevmode {\color {Chapter }Congruences}}{10}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}\leavevmode {\color {Chapter }CreamNumberOfBlocks}}{10}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}\leavevmode {\color {Chapter }CreamContainedPartition}}{10}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}\leavevmode {\color {Chapter }CreamPrincipalCongruence}}{11}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}\leavevmode {\color {Chapter }CreamAllPrincipalCongruences}}{11}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}\leavevmode {\color {Chapter }CreamIsAlgebraMonolithic}}{11}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}\leavevmode {\color {Chapter }CreamAllCongruences}}{12}{subsection.4.2.6}
\contentsline {section}{\numberline {4.3}\leavevmode {\color {Chapter }Isomorphisms, Automorphisms and Endomorphisms}}{13}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}\leavevmode {\color {Chapter }CreamAreIsomorphicAlgebras}}{13}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}\leavevmode {\color {Chapter }CreamAllSubUniverses}}{13}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}\leavevmode {\color {Chapter }CreamSubUniverse2Algebra}}{14}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}\leavevmode {\color {Chapter }CreamAlgebrasFromAxioms}}{14}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}\leavevmode {\color {Chapter }CreamEndomorphisms}}{14}{subsection.4.3.5}
\contentsline {subsection}{\numberline {4.3.6}\leavevmode {\color {Chapter }CreamAutomorphisms}}{15}{subsection.4.3.6}
\contentsline {subsection}{\numberline {4.3.7}\leavevmode {\color {Chapter }CreamExistsEpimorphism}}{15}{subsection.4.3.7}
\contentsline {subsection}{\numberline {4.3.8}\leavevmode {\color {Chapter }CreamOneEpimorphism}}{16}{subsection.4.3.8}
\contentsline {subsection}{\numberline {4.3.9}\leavevmode {\color {Chapter }CreamAllEpimorphisms}}{16}{subsection.4.3.9}
\contentsline {subsection}{\numberline {4.3.10}\leavevmode {\color {Chapter }CreamExistsMonomorphism}}{17}{subsection.4.3.10}
\contentsline {subsection}{\numberline {4.3.11}\leavevmode {\color {Chapter }CreamOneMonomorphism}}{17}{subsection.4.3.11}
\contentsline {subsection}{\numberline {4.3.12}\leavevmode {\color {Chapter }CreamAllMonomorphisms}}{18}{subsection.4.3.12}
\contentsline {subsection}{\numberline {4.3.13}\leavevmode {\color {Chapter }CreamExistsDivisor}}{18}{subsection.4.3.13}
\contentsline {subsection}{\numberline {4.3.14}\leavevmode {\color {Chapter }CreamOneDivisorUniverse}}{19}{subsection.4.3.14}
\contentsline {subsection}{\numberline {4.3.15}\leavevmode {\color {Chapter }CreamAllDivisorUniverses}}{19}{subsection.4.3.15}
\contentsline {subsection}{\numberline {4.3.16}\leavevmode {\color {Chapter }CreamExistsDirectFactorization}}{20}{subsection.4.3.16}
\contentsline {subsection}{\numberline {4.3.17}\leavevmode {\color {Chapter }CreamOneDirectFactorization}}{20}{subsection.4.3.17}
\contentsline {subsection}{\numberline {4.3.18}\leavevmode {\color {Chapter }CreamAllDirectFactors}}{20}{subsection.4.3.18}
\contentsline {chapter}{References}{22}{chapter*.5}
\contentsline {chapter}{Index}{23}{section*.6}
