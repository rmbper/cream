#############################################################################
##
#W    init.g     The Cream package         Rui Barradas Pereira
#W                                                       
##

#############################################################################
##
#R  Read the declaration files.
##
ReadPackage( "Cream", "lib/magmaut.gd" );
ReadPackage( "Cream", "lib/cream_iso.gd" );
ReadPackage( "Cream", "lib/Cream.gd" );

#E  init.g . . . . . . . . . . . . . . . . . . . . . . . . . . . .  ends here

