#############################################################################
##  
##  PackageInfo.g for the package `Cream'  Rui Barradas Pereira
##                                                       
##  (created from Example package PackageInfo.g file)
##  

##  <#GAPDoc Label="PKGVERSIONDATA">
##  <!ENTITY VERSION "2.0.15">
##  <!ENTITY COPYRIGHTYEARS "2024">
##  <!ENTITY RELEASEDATE "24/09/2024">
##  <!ENTITY ARCHIVENAME "cream">
##  <#/GAPDoc>

SetPackageInfo( rec(

PackageName := "Cream",
Subtitle := "Package that calculates congruences, endomorphisms and automorphisms for algebras of type (2^m,1^n)",
Version := "2.0.15",
Date := "24/09/2024",
PackageWWWHome := "https://gitlab.com/rmbper/cream/",
ArchiveURL := "https://gitlab.com/rmbper/cream/-/archive/master/cream-master.zip",
ArchiveFormats := ".zip",


Persons := [
  rec( 
    LastName      := "Pereira",
    FirstNames    := "Rui",
    IsAuthor      := true,
    IsMaintainer  := true,
    Email         := "rmbper@gmail.com",
    WWWHome       := "https://www.linkedin.com/in/ruibarradaspereira/",
    PostalAddress := Concatenation( [
                       "Department of Science and Technology\n",
                       "Universidade Aberta\n",
                       "Lisbon\n",
                       "Portugal" ] ),
    Place         := "Lisboa",
    Institution   := "Universidade Aberta"
  ),
  rec(
    LastName      := "Chow",
    FirstNames    := "Choiwah",
    IsAuthor      := true,
    IsMaintainer  := true,
    Email         := "choiwah.chow@gmail.com",
    WWWHome       := "https://www.linkedin.com/in/choiwah-chow-cfa-8ba8554/",
    PostalAddress := Concatenation( [
                       "Department of Science and Technology\n",
                       "Universidade Aberta\n",
                       "Lisbon\n",
                       "Portugal" ] ),
    Place         := "Lisboa",
    Institution   := "Universidade Aberta"
  ),
  rec(
    LastName      := "Sousa",
    FirstNames    := "Carlos",
    IsAuthor      := true,
    IsMaintainer  := true,
    Email         := "cfmsousa@gmail.com",
    PostalAddress := Concatenation( [
                       "Department of Science and Technology\n",
                       "Universidade Aberta\n",
                       "Lisbon\n",
                       "Portugal" ] ),
    Place         := "Lisboa",
    Institution   := "Universidade Aberta"
  ),
  rec( 
    LastName      := "Araújo",
    FirstNames    := "João",
    IsAuthor      := true,
    IsMaintainer  := false,
    Email         := "jj.araujo@fct.unl.pt",
    PostalAddress := Concatenation( [
                       "João Araújo\n",
                       "Mathematics Department\n",
                       "Faculty of Science and Technology\n",
                       "Universidade Nova de Lisboa\n",
                       "Portugal" ] ),
    Place         := "Lisboa",
    Institution   := "Universidade Nova de Lisboa"
  ),
   rec( 
    LastName      := "Bentz",
    FirstNames    := "Wolfram",
    IsAuthor      := true,
    IsMaintainer  := false,
    Email         := "wolfram.bentz@uab.pt",
    PostalAddress := Concatenation( [
                       "Wolfram Bentz\n",
                       "Department of Science and Technology\n",
                       "Universidade Aberta\n",
                       "Lisbon\n",
                       "Portugal" ] ),
    Place         := "Lisbon",
    Institution   := "Universidade Aberta"
  ),
  rec( 
    LastName      := "Sequeira",
    FirstNames    := "Luis",
    IsAuthor      := true,
    IsMaintainer  := false,
    Email         := "lfsequeira@fc.ul.pt",
    PostalAddress := Concatenation( [
                     "Luís Sequeira\n",	
                     "Faculdade de Ciências da Universidade de Lisboa\n",
                     "Av. Prof. Gama Pinto, 2\n",
                     "1649-003 Lisboa\n",
                     "Portugal" ] ),
    Place         := "Lisboa",
    Institution   := "Universidade de Lisboa"
     ),  
  
],

Status := "dev",

README_URL := 
  Concatenation( ~.PackageWWWHome, "README" ),
PackageInfoURL := 
  Concatenation( ~.PackageWWWHome, "PackageInfo.g" ),

AbstractHTML := 
  "The <span class=\"pkgname\">Cream</span> package, calculates \
    congruences, endomorphisms and automorphisms for algebras of type (2^m,1^n) \
    Based on 'Computing congruences efficiently' by Ralph Freese",

PackageDoc := rec(
  BookName  := "Cream",
  ArchiveURLSubset := ["doc"],
  HTMLStart := "doc/chap0.html",
  PDFFile   := "doc/manual.pdf",
  SixFile   := "doc/manual.six",
  LongTitle := "CREAM / Algebra <E>C</E>ong<E>R</E>uences, <E>E</E>ndomorphisms and <E>A</E>utomorphis<E>M</E>s",
),


Dependencies := rec(
  GAP := "4.8.6",
  NeededOtherPackages := [],
 SuggestedOtherPackages := [],
  ExternalConditions := []
                      
),

AvailabilityTest := ReturnTrue,

BannerString := Concatenation( 
    "----------------------------------------------------------------\n",
    "Loading  Cream ", ~.Version, "\n",
    "by ",
    JoinStringsWithSeparator( List( Filtered( ~.Persons, r -> r.IsAuthor ),
                                    r -> Concatenation(
        r.FirstNames, " ", r.LastName, " (", r.Email, ")\n" ) ), "   " ),
    "For help, type: ?Cream package \n",
    "----------------------------------------------------------------\n" ),

TestFile := "tst/testall.tst",

Keywords := ["universal algebra", "congruences", "endomorphisms", "automorphisms"]

));

