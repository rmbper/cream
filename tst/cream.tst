#############################################################################
##
#A  cream.tst            CREAM package                Rui Barradas Pereira
##
## 
gap> START_TEST("CREAM package: cream.tst");

gap> s3:= [MultiplicationTable(SymmetricGroup (3))];
[ [ [ 1, 2, 3, 4, 5, 6 ], [ 2, 1, 4, 3, 6, 5 ], [ 3, 5, 1, 6, 2, 4 ], 
      [ 4, 6, 2, 5, 1, 3 ], [ 5, 3, 6, 1, 4, 2 ], [ 6, 4, 5, 2, 3, 1 ] ] ]
gap> s2:= [MultiplicationTable(SymmetricGroup (2))];
[ [ [ 1, 2 ], [ 2, 1 ] ] ]
gap> z6:= [MultiplicationTable(CyclicGroup (6))];
[ [ [ 1, 2, 3, 4, 5, 6 ], [ 2, 3, 4, 5, 6, 1 ], [ 3, 4, 5, 6, 1, 2 ], 
      [ 4, 5, 6, 1, 2, 3 ], [ 5, 6, 1, 2, 3, 4 ], [ 6, 1, 2, 3, 4, 5 ] ] ]
gap> v4:=[MultiplicationTable(DirectProduct(SymmetricGroup(2),SymmetricGroup(2)))];
[ [ [ 1, 2, 3, 4 ], [ 2, 1, 4, 3 ], [ 3, 4, 1, 2 ], [ 4, 3, 2, 1 ] ] ]
gap> CreamAllCongruences (s3);                           
[ [ -1, -1, -1, -1, -1, -1 ], [ -3, -3, 2, 1, 1, 2 ], [ -6, 1, 1, 1, 1, 1 ] ]
gap> CreamIsAlgebraMonolithic(s3);
true
gap> CreamAreIsomorphicAlgebrasMace4(s3,z6);  
false
gap> CreamAreIsomorphicAlgebras(s3,z6);
false
gap> axioms := "x * x = x. (x * y) * x = x * y.y * (x * y) = x * y. x * ((x * y) * z) = (x * y) * z.";
"x * x = x. (x * y) * x = x * y.y * (x * y) = x * y. x * ((x * y) * z) = (x * y) * z."
gap> CreamAutomorphisms(s3);                
[ [ 1 .. 6 ], [ 1, 2, 6, 5, 4, 3 ], [ 1, 3, 2, 5, 4, 6 ], 
  [ 1, 3, 6, 4, 5, 2 ], [ 1, 6, 2, 4, 5, 3 ], [ 1, 6, 3, 5, 4, 2 ] ]
gap> CreamEndomorphisms (s3);
[ [ 1, 1, 1, 1, 1, 1 ], [ 1, 2, 2, 1, 1, 2 ], [ 1, 2, 3, 4, 5, 6 ], 
  [ 1, 2, 6, 5, 4, 3 ], [ 1, 3, 2, 5, 4, 6 ], [ 1, 3, 3, 1, 1, 3 ], 
  [ 1, 3, 6, 4, 5, 2 ], [ 1, 6, 2, 4, 5, 3 ], [ 1, 6, 3, 5, 4, 2 ], 
  [ 1, 6, 6, 1, 1, 6 ] ]
gap> sas := CreamAllSubUniverses(s3);
[ [ 1 ], [ 1, 2 ], [ 1, 2, 3, 4, 5, 6 ], [ 1, 3 ], [ 1, 4, 5 ], [ 1, 6 ] ]
gap> CreamSubUniverse2Algebra(s3, sas[2]);
[ [ [ 1, 2 ], [ 2, 1 ] ] ]
gap> CreamAllEpimorphisms(s3,s2);            
[ [ 1, 2, 2, 1, 1, 2 ] ]
gap> CreamAllMonomorphisms(s2,s3);
[ [ 1, 2 ], [ 1, 3 ], [ 1, 6 ] ]
gap> CreamAllDivisorUniverses(s3,s2);            
[ [ [ 1, 2 ], [ -1, -1 ] ], [ [ 1, 2, 3, 4, 5, 6 ], [ -3, -3, 2, 1, 1, 2 ] ], 
  [ [ 1, 3 ], [ -1, -1 ] ], [ [ 1, 6 ], [ -1, -1 ] ] ]
gap> CreamAllDirectFactors(v4);                                                
[ [ [ -2, 1, -2, 3 ], [ -2, -2, 2, 1 ] ], 
  [ [ -2, 1, -2, 3 ], [ -2, -2, 1, 2 ] ], 
  [ [ -2, -2, 2, 1 ], [ -2, -2, 1, 2 ] ] ]


gap> STOP_TEST( "cream.tst", 10000 );

#############################################################################
##
#E
