#############################################################################
##
#W    read.g     The Cream package         Rui Barradas Pereira
#W                                                       
##

#############################################################################
##
#R  Read the install files.
##
LoadDynamicModule(Filename(DirectoriesPackagePrograms("Cream"),"cCream.la.so"));
ReadPackage( "Cream", "lib/magmaut.gi" );
ReadPackage( "Cream", "lib/cream_iso.gi" );
ReadPackage( "Cream", "lib/Cream.gi" );

#E  read.g . . . . . . . . . . . . . . . . . . . . . . . . . . . .  ends here

